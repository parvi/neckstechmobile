import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

/*
  Generated class for the LoadingControllerServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoadingControllerServicesProvider {

  loader:any
  constructor(public loadingCtrl: LoadingController){
    //creates loader once so there is less data redundancy
  }
  showLoader() { //call this fn to show loader
      this.loader = this.loadingCtrl.create({
          content: `loading...`,
      });
      this.loader.present(); 
      return this.loader;   
  }

}
