import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServicesProvider } from './services';

/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VilleProvider {
  
  constructor(public http: HttpClient,
              public services: ServicesProvider) {

  }

  private url = this.services.url;
  listVilleByRegionID(value:number){
    return this.http.post<any>(this.url+'/villes-par-region', value);
  }
  listVille(){
    return this.http.get<any>(this.url+'/list-ville');
  }

}
