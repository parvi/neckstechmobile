import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServicesProvider } from './services';

/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RendezvousProvider {
  
  constructor(public http: HttpClient,
              public services: ServicesProvider) {

  }
   private _url:string = this.services.url;
  //connection
  ajoutRendezvous(value: any) {
    return this.http.post(this._url + '/rendezvous', value);
  }
  //infos user
  ListRendezvous() {
    return this.http.get<any>(this._url + '/rendezvous');
  }
  ListRendezvousByProspect(value:any) {
    return this.http.get<any>(this._url + '/rendezvous/prospect/'+value);
  }
  ListRendezvousByProspectEncours(value:any) {
    return this.http.get<any>(this._url + '/rendezvous/encours/prospect/'+value)
  }
  updateRv(value:any) {
    return this.http.put<any>(this._url + '/rendezvous',value);
  }


}
