import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { ServicesProvider } from './services';
import { EventEmitterService } from './EventEmitter';
import { ModulestockProvider } from './modulestock/modulestock';
import { Categorie } from '../app/models/model.categorie';

/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TestService {

    constructor(private servicesModuleStock: ModulestockProvider, public serviceevent: EventEmitterService) {

    }
    checkSouscategorie(listInput) {

        if (listInput[3].ngvar) {
            listInput[4].showSpinnerCat = true;
        } else {
            listInput[4].showSpinnerCat = false;
        }

        if (!listInput[4].ngvar) {
            listInput[4].ngvar = this.serviceevent.getListCat();

        }
        return listInput;
    }
    checkCommande(listInput) {

        if (listInput[3].ngvar) {
            listInput[4].showSpinnerCat = true;
        } else {
            listInput[4].showSpinnerCat = false;
        }

        if (!listInput[4].ngvar) {
            listInput[4].ngvar = this.serviceevent.getListComm();

        }
        return listInput;
    }

    checkObject(listInput, index, modulev, field) {
       /* console.log(listInput[index].placehorlder)
        console.log(listInput[index].ngvar)
        console.log(index)*/
        if (listInput[index].ngvar) {
            listInput[index + 1].showSpinnerCat = true;
        } else {
            listInput[index + 1].showSpinnerCat = false;
        }

        if (!listInput[index + 1].ngvar) {
            switch (modulev) {
                case 'categorie':
                    listInput[index + 1].ngvar = this.serviceevent.getListCat();
                    break;
                case 'commande':
                    listInput[index + 1].ngvar = this.serviceevent.getListComm();
                    break;
                case 'mouvement':
                    if (field == "Client") {
                        listInput[index + 1].ngvar = this.serviceevent.getListCli();
                    } else if (field == "Fournisseur") {
                        listInput[index + 1].ngvar = this.serviceevent.getListFour();
                    }
                    break;
            }

        }
        return listInput;
    }

    checkListObject(listInput, item: any, index: number) {

        listInput[index].item = item
        console.log(index)
        return listInput;
    }
    listCategorie() {
        this.servicesModuleStock.listCategories(20, 0).
            subscribe(response => {
                if (response) {
                    if (response['content']) {
                        this.serviceevent.addListCategories(response['content']);
                    }
                }

            }, err => {
                console.log(err)
                this.serviceevent.addListCategories(null);
            });
    }


}
