import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, ToastController } from 'ionic-angular';
import { Prospect } from '../../app/models/prospect.model';

/*
  Generated class for the NotificationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NotificationsProvider {

  constructor(public http: HttpClient,public alertCtrl : AlertController, public toast: ToastController) {
    console.log('Hello NotificationsProvider Provider');
  }

async presentConfirm(header: any,message: any,cancelText: any,okText: any): Promise<any> {
  return new Promise(async (resolve) => {
    const alert = await this.alertCtrl.create({
      title: header,
      message: message,
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (cancel) => {
            resolve('cancel');
          }
        }, {
          text: okText,
          handler: (ok) => {
            resolve('ok');
          }
        }
      ]
    });
    alert.present();
  });
}

presentToastMessage(message: string) {
  const toast = this.toast.create({
    position: 'top',
    message: message,
    showCloseButton: true,
    closeButtonText: "OK",
    duration: 3000
  });
  toast.present();
}
}
