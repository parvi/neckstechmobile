import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServicesProvider } from './services';

/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProspectProvider {
  
  constructor(public http: HttpClient,
              public services: ServicesProvider) {

  }
   private _url:string = this.services.url;
  //connection
  ajoutProspect(value: any) {
    return this.http.post(this._url + '/prospects', value);
  }
  //infos user
  ListProspect() {
    return this.http.get<any>(this._url + '/prospects');
  }
  


}
