import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServicesProvider } from '../services';
import { resolveDefinition } from '@angular/core/src/view/util';
import { Categorie } from '../../app/models/model.categorie';
import { Produit } from '../../app/models/model.produit';
import { Mouvement } from '../../app/models/model.mouvement';

/*
  Generated class for the NotificationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ModulestockProvider {

  constructor(public http: HttpClient, public services: ServicesProvider) {
    console.log('Hello NotificationsProvider Provider');
  }
  private _url: string = this.services.url;
  //Ajout categorie
  addCategorie(value: any) {
    return this.http.post(this._url + '/categories', value);
  }
  listCategories(size: number, page: number) {
    return this.http.get(this._url + '/categories?' + 'size=' + size + '&page=' + page);
  }
  addProduit(value: any) {
    return this.http.post(this._url + '/produits', value);
  }
  listProduits(size: number, page: number) {
    var reponse: any;
    if (size == 0 && page == 0) {
      reponse = this.http.get(this._url + '/produits?' + 'size=' + size + '&page=' + page);
    } else {
      reponse = this.http.get(this._url + '/produits');
    }
    return reponse;

  }

  addMouvement(value: any) {
    return this.http.post(this._url + '/mouvements', value);
  }
  listMouvement(size: number, page: number) {
    if (size == 0 && page == 0) {
      return this.http.get(this._url + '/mouvements');
    } else {
      return this.http.get(this._url + '/mouvements?' + 'size=' + size + '&page=' + page);
    }

  }
  async newaddCategorie(categorie: Categorie) {
    return new Promise(async (resolve) => {
      await this.http.post(this._url + '/categories', categorie)
        .subscribe(result => {
          if (result) {
            resolve(result)
          } else {
            resolve(null)
          }
        }, err => {
          resolve(null)
        })
    })
  }
  async newaddProduit(produit: Produit) {
    return new Promise(async (resolve) => {
      await this.http.post(this._url + '/produits', produit)
        .subscribe(result => {
          if (result) {
            resolve(result)
          } else {
            resolve(null)
          }
        }, err => {
          resolve(null)
        })
    })
  }
  async newaddMouvement(mouvement: Mouvement) {
    return new Promise(async (resolve) => {
      await this.http.post(this._url + '/mouvements', mouvement)
        .subscribe(result => {
          if (result) {
            resolve(result)
          } else {
            resolve(null)
          }
        }, err => {
          resolve(null)
        })
    })
  }
  async newlistMouvement(size: number, page: number) {
    return new Promise(async (resolve) => {
      if (size == 0 && page == 0) {
        await this.http.get(this._url + '/mouvements')
          .subscribe(result => {
            if (result) {
              resolve(result);
            } else {
              resolve(null);
            }

          }, err => {
            resolve(null);
          })
      } else {
        await this.http.get(this._url + '/mouvements?' + 'size=' + size + '&page=' + page)
          .subscribe(result => {
            if (result) {
              resolve(result);
            } else {
              resolve(null);
            }

          }, err => {
            resolve(null);
          })
      }
    })

  }
  async  newlistProduits(size: number, page: number) {
    return new Promise(async (resolve) => {
    if (size == 0 && page == 0) {
      await   this.http.get(this._url + '/produits?' + 'size=' + size + '&page=' + page)
      .subscribe(result => {
        if (result) {
          resolve(result);
        } else {
          resolve(null);
        }
      }, err => {
        resolve(null);
      })
    } else {
      await this.http.get(this._url + '/produits')
      .subscribe(result => {
        if (result) {
          resolve(result);
        } else {
          resolve(null);
        }
      }, err => {
        resolve(null);
      })
    }
  })
   
  }
  
}
