import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { ServicesProvider } from '../services';
import { Categorie } from '../../app/models/model.categorie';
import { EventEmitterService } from '../EventEmitter';
import { ModulestockProvider } from './modulestock';
import { Produit } from '../../app/models/model.produit';
import { ResourceLoader } from '@angular/compiler';
import { CommandesProvider } from '../commandes/commandeservices';
import { Commande } from '../../app/models/model.commande';
import { LineMouvement } from '../../app/models/model.linemouvement';
import { Mouvement } from '../../app/models/model.mouvement';
import { Client } from '../../app/models/model.client';
import { Fournisseur } from '../../app/models/model.fournisseur';
import { LineCommande } from '../../app/models/model.linecommande';

/*
  Generated class for the NotificationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilsStockProvider {

    constructor(public http: HttpClient,
        public services: ServicesProvider,
        public eventemitter: EventEmitterService,
        public servicesModuleStock: ModulestockProvider,
        public serviceCommande: CommandesProvider
    ) {
        console.log('Hello NotificationsProvider Provider');
    }
    controlFormCategorie(listfield) {
        var result: string = '1';
        for (let i of listfield) {
            if (i.module == null) {
                if (i.placehorlder !== "Sous categorie" && i.placehorlder !== "Parent") {
                    if (i.ngvar == "" || i.ngvar == null) {
                        result = i.placehorlder;
                        break;
                    } else {

                    }
                }
            }
            // console.log(listfield)

        }
        return result;
    }
    controlFormProduit(listfield) {
        var result: string = '1';
        for (let i of listfield) {
            if (i.module == null) {
                //    if (i.placehorlder !== "Sous categorie" && i.placehorlder !== "Parent") {
                if (i.field == 'select-searchable') {
                    if (i.item == null) {
                        result = i.placehorlder;
                        break;
                    }
                } else {
                    if (i.ngvar == "" || i.ngvar == null) {
                        result = i.placehorlder;
                        break;
                    } else {

                    }
                }

                //  }
            }
            // console.log(listfield)

        }
        return result;
    }
    controlFormMouvement(listfield) {
        var result: string = '1';
        for (let i of listfield) {
            if (i.module == null) {
                if (i.field == 'select-searchable' && i.placehorlder !== 'Commandes' && i.placehorlder !== "Produits") {
                    if (i.item == null) {
                        result = i.placehorlder;
                        break;
                    }
                } else if (i.field == 'input' && i.placehorlder !== 'Quantité') {
                    if (i.ngvar == "" || i.ngvar == null) {
                        result = i.placehorlder;
                        break;
                    }
                } else if (i.field == 'details') {
                    if (i.ngvar == "" || i.ngvar == []) {
                        result = 'Ajout de produit';
                    }
                }

            }

        }
        return result;
    }
    controlFormCommande(listfield) {
        var result: string = '1';
        for (let i of listfield) {
            if (i.module == null) {
                if (i.field == 'select-searchable' && i.placehorlder !== "Produits") {
                    var retour = this.controlClientOrFournisseur(listfield)
                    if (retour !== "oui") {
                        result = retour;
                        break;
                    }
                    /*   if (i.item == null) {
                           result = i.placehorlder;
                           break;
                       }*/
                } else if (i.field == 'input' && i.placehorlder !== 'Quantité') {
                    if (i.ngvar == "" || i.ngvar == null) {
                        result = i.placehorlder;
                        break;
                    }
                } else if (i.field == 'details') {
                    if (i.ngvar == "" || i.ngvar == []) {
                        result = 'Ajout de produit';
                    }
                }

            }

        }
        return result;
    }
    controlClientOrFournisseur(listfield) {
        var retour: string = "oui";
        if (listfield[2].item == null && listfield[4].item == null) {
            retour = "Client ou Fournisseur";
        }
        return retour;
    }
    controlFormDetailsProduit(listfield) {
        var result: string = '1';
        for (let i of listfield) {
            if (i.module == null) {
                if (i.field == 'select-searchable' && i.placehorlder == "Produits") {
                    if (i.item == null) {
                        result = i.placehorlder;
                        break;
                    }
                } else if (i.field == 'input' && i.placehorlder == 'Quantité') {
                    if (i.ngvar == "" || i.ngvar == null) {
                        result = i.placehorlder;
                        break;
                    }
                }

            }

        }
        return result;
    }
    initFormCategorie() {
        var listInput: Array<{}> = [
        ];
        var categories: Categorie[] = null;
        categories = this.eventemitter.getListCat();
        listInput.push(
            { module: 'categorie' },
            { ngvar: "", placehorlder: 'Nom de la catégorie', type: 'text', id: 'prospect', field: 'input' },
            { ngvar: "", placehorlder: 'Description de la catégorie', type: 'text', id: 'adresse', field: 'input' },
            { ngvar: false, placehorlder: 'Sous categorie', type: '', id: 'souscategorie', field: 'checkbox' },
            { ngvar: categories, placehorlder: 'Parent', type: '', id: 'listsouscategorie', field: 'select-searchable', showSpinnerCat: false, item: null, nom: 'nom' },
        );
        return listInput;
    }
    initFormProduit() {
        var listInput: Array<{}> = [
        ];
        var categories: Categorie[] = null;
        categories = this.eventemitter.getListCat();
        listInput.push(
            { module: 'produit' },
            { ngvar: "", placehorlder: 'libelle du produit', type: 'text', id: 'libelle', field: 'input' },
            { ngvar: "", placehorlder: 'Description du produit', type: 'text', id: 'description', field: 'input' },
            { ngvar: "", placehorlder: 'Prix unitaire', type: 'number', id: 'pu', field: 'input' },
            { ngvar: "", placehorlder: 'Prix de vente', type: 'number', id: 'pv', field: 'input' },
            { ngvar: "", placehorlder: 'quantite', type: 'number', id: 'quantite', field: 'input' },
            { ngvar: "", placehorlder: 'seuil', type: 'number', id: 'seuil', field: 'input' },
            { ngvar: categories, placehorlder: 'Categorie', type: '', id: 'listsouscategorie', field: 'select-searchable', showSpinnerCat: true, item: null, nom: 'nom' },
        );
        return listInput;
    }
    
    initFormMouvement() {
        var listInput: Array<{}> = [
        ];
        var commandes: Commande[] = null;
        commandes = this.eventemitter.getListComm();
        var produits: Produit[] = null;
        produits = this.eventemitter.getListProd();
        var types: any[] = [{ id: 1, nom: 'entrée' },
        { id: 1, nom: 'sortie' },];
        var details: LineMouvement = new LineMouvement();
        listInput.push(
            { module: 'mouvement' },
            { ngvar: "", placehorlder: 'Description', type: 'text', id: 'libelle', field: 'input' },
            { ngvar: types, placehorlder: 'Type', type: '', id: 'listtype', field: 'select-searchable', showSpinnerCat: true, item: null, nom: 'nom' },
            { ngvar: false, placehorlder: 'Commande', type: '', id: 'commande', field: 'checkbox' },
            { ngvar: commandes, placehorlder: 'Commandes', type: '', id: 'listcommandes', field: 'select-searchable', showSpinnerCat: false, item: null, nom: 'createdDate' },
            { ngvar: produits, placehorlder: 'Produits', type: '', id: 'listproduits', field: 'select-searchable', showSpinnerCat: true, item: null, nom: 'libelle' },
            { ngvar: "", placehorlder: 'Quantité', type: 'number', id: 'qt', field: 'input' },
            { ngvar: [], placehorlder: 'details', type: 'details', id: 'details', field: 'details' },
            { ngvar: 0, placehorlder: 'total', type: '', id: 'detailsqt', field: 'detailsqt' }


        );
        return listInput;
    }
    initFormCommande() {
        var listInput: Array<{}> = [
        ];
        var clients: Client[] = null;
        clients = this.eventemitter.getListCli();
        var fournisseurs: Fournisseur[] = null;
        fournisseurs = this.eventemitter.getListFour();
        var produits: Produit[] = null;
        produits = this.eventemitter.getListProd();
        listInput.push(
            { module: 'commande' },
            //     { ngvar: "", placehorlder: 'Description', type: 'text', id: 'libelle', field: 'input' },
            //   { ngvar: types, placehorlder: 'Type', type: '', id: 'listtype', field: 'select-searchable', showSpinnerCat: true, item: null, nom: 'nom' },
            { ngvar: false, placehorlder: 'Client', type: '', id: 'client', field: 'checkbox' },
            { ngvar: clients, placehorlder: 'Clients', type: '', id: 'listclients', field: 'select-searchable', showSpinnerCat: false, item: null, nom: 'nom_complet' },
            { ngvar: false, placehorlder: 'Fournisseur', type: '', id: 'fournisseur', field: 'checkbox' },
            { ngvar: fournisseurs, placehorlder: 'Fournisseurs', type: '', id: 'listfournisseurs', field: 'select-searchable', showSpinnerCat: false, item: null, nom: 'firstName' },
            { ngvar: produits, placehorlder: 'Produits', type: '', id: 'listproduits', field: 'select-searchable', showSpinnerCat: true, item: null, nom: 'libelle' },
            { ngvar: "", placehorlder: 'Quantité', type: 'number', id: 'qt', field: 'input' },
            { ngvar: [], placehorlder: 'details', type: 'details', id: 'details', field: 'details' },
            { ngvar: 0, placehorlder: 'total', type: '', id: 'detailsqt', field: 'detailsqt' }


        );
        return listInput;
    }
    ajoutCategorie(listfield) {
        return new Promise(async (resolve) => {
            await this.servicesModuleStock.newaddCategorie(this.initObjectCategorie(listfield))
                .then(result => {
                    resolve(result);
                })
        })
    }
    ajoutProduit(listfield) {
        return new Promise(async (resolve) => {
            await this.servicesModuleStock.newaddProduit(this.initObjectProduit(listfield))
                .then(result => {
                    resolve(result);
                })
        })
    }
    ajoutMouvement(listfield) {

        return new Promise(async (resolve) => {

            await this.servicesModuleStock.newaddMouvement(this.initObjectMouvement(listfield))
                .then(result => {
                    resolve(result);
                })
        })
    }
    ajoutCommande(listfield) {

        return new Promise(async (resolve) => {
            await this.serviceCommande.newaddCommande(this.initObjectCommande(listfield))
                .then(result => {
                    resolve(result);
                })
        })
    }
    initObjectCategorie(listfield: any) {
        var categorie: Categorie = new Categorie();
        categorie.nom = listfield[1].ngvar;
        categorie.description = listfield[2].ngvar;
        categorie.categorie = listfield[4].item;
        return categorie;
    }
    initObjectProduit(listfield: any) {
        var produit: Produit = new Produit();
        produit.libelle = listfield[1].ngvar;
        produit.description = listfield[2].ngvar;
        produit.prixUnitaire = listfield[3].ngvar;
        produit.prixVente = listfield[4].ngvar;
        produit.quantite = listfield[5].ngvar;
        produit.seuil = listfield[6].ngvar;
        produit.categorie = listfield[7].item;
        return produit;
    }
    initObjectMouvement(listfield: any) {
        var mouvement: Mouvement = new Mouvement();
        mouvement.description = listfield[1].ngvar
        mouvement.type = listfield[2].item.nom
        mouvement.commande = listfield[4].item
        mouvement.details = listfield[7].ngvar
        mouvement.total = listfield[8].ngvar
        console.log(mouvement)
        return mouvement;
    }
    initObjectCommande(listfield: any) {
        var commande: Commande = new Commande();
        commande.client = listfield[2].item;
        commande.fournisseur = listfield[4].item;
        commande.details = listfield[7].ngvar;
        commande.total = listfield[8].ngvar;
        if(listfield[2].item){

        }else{
            
        }
        commande.type = "Client"
        console.log(commande)
        return commande;
    }
    listProduit() {
        this.servicesModuleStock.newlistProduits(0, 0)
            .then(result => {
                var produits: Produit[] = [];
                produits = result['content'];
                this.eventemitter.addListProduits(produits);
            })
    }
    listCommandes() {
        this.serviceCommande.newlistCommandes(0, 0)
            .then(result => {
                var commandes: Commande[] = [];
                commandes = result['content'];
                this.eventemitter.addListCommandes(commandes);
            })
    }
    listClient() {
        this.serviceCommande.newlistClient(0, 0)
            .then(result => {
                var clients: Client[] = [];
                clients = result['content'];
                this.eventemitter.addListClients(clients);
            })
    }
    listFournisseur() {
        this.serviceCommande.newlistFournisseur(0, 0)
            .then(result => {
                var fournisseurs: Fournisseur[] = [];
                fournisseurs = result['content'];
                this.eventemitter.addListFournisseurs(fournisseurs);
            })
    }
    ajoutDetailsProduit(listfield: any) {
        var details: any;
        details = this.initObjectDetailsProduit(listfield);
        var list: any = listfield[7].ngvar;
        var quantite: number = listfield[8].ngvar;
        if (list == null || list.length == 0) {
            list = [];
            list.push(details)
        } else {
            list.push(details)
        }
        quantite += parseInt(details.quantite + "");
        listfield[7].ngvar = list;
        listfield[8].ngvar = quantite;
        return listfield
    }
    initObjectDetailsProduit(listfield: any) {
        var detailss: any;
        switch (listfield[0].module) {
            case "mouvement":
                var details: LineMouvement = new LineMouvement();
                detailss = this.detailsForModule(listfield, details);
                break;
            case "commande":
                var details: LineCommande = new LineCommande();
                detailss = this.detailsForModule(listfield, details);
                break;
        }
        return detailss;
    }
    detailsForModule(listfield, details) {
        console.log(listfield);
        details.produit = listfield[5].item;
        details.quantite = parseInt(listfield[6].ngvar + "");
        details.prix = listfield[5].item.prixVente;
        return details;
    }
    


}