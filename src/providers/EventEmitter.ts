import { HttpClient } from '@angular/common/http';
import { Injectable, Input, Output, EventEmitter } from '@angular/core';
import { ServicesProvider } from './services';
import { Categorie } from '../app/models/model.categorie';
import { Produit } from '../app/models/model.produit';
import { Commande } from '../app/models/model.commande';
import { Client } from '../app/models/model.client';
import { Fournisseur } from '../app/models/model.fournisseur';

/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EventEmitterService {
    @Output() listInput: EventEmitter<Array<{}>> = new EventEmitter();
    @Output() listCategories: EventEmitter<Categorie[]> = new EventEmitter();
    @Output() listProduits: EventEmitter<Produit[]> = new EventEmitter();
    @Output() listCommandes: EventEmitter<Commande[]> = new EventEmitter();
    @Output() listClients: EventEmitter<Client[]> = new EventEmitter();
    @Output() listFournisseurs: EventEmitter<Fournisseur[]> = new EventEmitter();
    @Output() update: EventEmitter<boolean> = new EventEmitter();
    listInputt: Array<{}>;
    listcategories: Categorie[];
    listproduits: Produit[];
    listcommandes: Commande[];
    listclients: Client[];
    listfournisseurs: Fournisseur[];
    constructor() {

    }
    //Event for modul input
    addListInput(list: Array<{}>) {
        //return list;
        this.listInput.emit(list);
        this.setListInput(list);
    }

    setListInput(list: Array<{}>) {
        this.listInput.emit(list);
        this.listInputt = list;
    }
    getListInput() {
        return this.listInputt;
    }
    //Event for module categorie
    addListCategories(list: Categorie[]) {
        this.listCategories.emit(list);
        this.setListCat(list);
    }
    setListCat(list: Categorie[]) {
        this.listCategories.emit(list);
        this.listcategories = list;
    }
    getListCat() {
        return this.listcategories;
    }

    //Event for module produit
    addListFournisseurs(list: Fournisseur[]) {
        console.log(list);
        this.listFournisseurs.emit(list);
        this.setListFour(list);
    }
    setListFour(list: Fournisseur[]) {
        this.listFournisseurs.emit(list);
        this.listfournisseurs= list;
    }
    getListFour() {
        console.log(this.listfournisseurs)
        return this.listfournisseurs;
    }
     //Event for module produit
     addListClients(list: Client[]) {
        console.log(list);
        this.listClients.emit(list);
        this.setListCli(list);
    }
    setListCli(list: Client[]) {
        this.listClients.emit(list);
        this.listclients = list;
    }
    getListCli() {
        console.log(this.listclients)
        return this.listclients;
    }
    //Event for module commande
    addListCommandes(list: Commande[]) {
        console.log(list)
        this.listCommandes.emit(list);
        this.setListComm(list);
    }
    setListComm(list: Commande[]) {
        this.listCommandes.emit(list);
        this.listcommandes = list;
    }
    getListComm() {
        return this.listcommandes;
    }
       //Event for module client
       addListProduits(list: Produit[]) {
        console.log(list);
        this.listProduits.emit(list);
        this.setListProd(list);
    }
    setListProd(list: Produit[]) {
        this.listProduits.emit(list);
        this.listproduits = list;
    }
    getListProd() {
        return this.listproduits;
    }


}
