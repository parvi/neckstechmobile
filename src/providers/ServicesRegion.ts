import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServicesProvider } from './services';

/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RegionProvider {
  
  constructor(public http: HttpClient,
              public services: ServicesProvider) {

  }

  private url = this.services.url;
  listRegion(){
    return this.http.get<any>(this.url+'/list-region');
  }

}
