import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';

/*
  Generated class for the ConnectivityServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConnectivityServiceProvider {
  onDevice: boolean;
  constructor(public platform: Platform, public network: Network) {
    this.onDevice = this.platform.is('cordova');
  }

  public statConnection(){
    if(this.isOnline()) {
     
      return true;
    } else {
    
      return false;
      
    }
  }
  private isOnline(): boolean {
  
    return this.network.type !== 'none';
  }
 
  private isOffline(): boolean {
    
    return this.network.type === 'none';
  }

}
