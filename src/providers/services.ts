import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServicesProvider {
  private _url1 = 'http://v-beta.diiman.com:8080/diiman/graphql';
  private _url = 'http://v-beta.diiman.com:8080/diiman/api';
  /*private _url1 = 'http://erp.diiman.com:8080/diiman/graphql';
  private _url = 'http://erp.diiman.com:8080/diiman/api';*/
 /* private _url = 'http://192.168.0.114:8080/api';
  private _url1 = 'http://192.168.0.114:8080/graphql';*/
  /*private _url = 'http://localhost:8080/api';
  private _url1 = 'http://localhost:8080/graphql';*/
  constructor(public http: HttpClient) {

  } 

  //connection
  login(value: any) {
    return this.http.post(this._url + '/login', value);
  }
  //infos user
  getUser() {
    return this.http.get<any>(this._url + '/utilisateur-connecte');
  }

  get url(): string {
    return this._url;
  }

  set url(value: string) {
    this._url = value;
  }
  get url1(): string {
    return this._url1;
  }

  set url1(value: string) {
    this._url1 = value;
  }

}
