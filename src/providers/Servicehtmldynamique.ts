import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { ServicesProvider } from './services';
import { EventEmitterService } from './EventEmitter';
import { TestService } from './testservice';
import { Categorie } from '../app/models/model.categorie';
import { UtilsStockProvider } from './modulestock/utilsstock';

/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HtmlDynamique {
    listInput: Array<{}> = [
    ];
    constructor(private eventemitter: EventEmitterService, private serviceTest: TestService, public utilsStock: UtilsStockProvider) {

    }

    public FormByModule(module: string) {
        switch (module) {
            case 'test':
                var listInput: Array<{}> = [
                ];
                listInput.push(
                    { module: 'Test' },
                    { ngvar: "", placehorlder: 'Nom', type: 'text', id: 'nom', field: 'input' },
                    { ngvar: "", placehorlder: 'Prenom', type: 'text', id: 'prenom', field: 'input' },
                    { ngvar: "", placehorlder: 'Adresse', type: 'text', id: 'adresse', field: 'input' },
                    { ngvar: "", placehorlder: 'mot de passe', type: 'password', id: 'motdepasse', field: 'input' },
                )

                this.eventemitter.addListInput(listInput);
                break;
            case 'Catégories':
                this.eventemitter.addListInput(this.utilsStock.initFormCategorie());
                break;
            case 'Produits':
                this.eventemitter.addListInput(this.utilsStock.initFormProduit());
                break;
            case 'Mouvements':
                this.eventemitter.addListInput(this.utilsStock.initFormMouvement());
                break;
            case 'Commandes':
                this.eventemitter.addListInput(this.utilsStock.initFormCommande());
                break;
        }
    }

}
