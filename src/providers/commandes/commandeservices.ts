import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServicesProvider } from '../services';
import { Commande } from '../../app/models/model.commande';

@Injectable()
export class CommandesProvider {
  constructor(public http: HttpClient, public services: ServicesProvider) {
    console.log('Hello NotificationsProvider Provider');
  }

  private _url: string = this.services.url;

  addCommande(value: Commande) {
    return this.http.post(this._url + '/commandes', value);
  }
  listCommandes(size: number, page: number) {
    if (size == 0 && page == 0) {
      return this.http.get(this._url + '/commandes');
    } else {
      return this.http.get(this._url + '/commandes?' + 'size=' + size + '&page=' + page);
    }

  }
  listClient(size: number, page: number) {
    if (size == 0 && page == 0) {
      return this.http.get(this._url + '/clients');
    } else {
      return this.http.get(this._url + '/clients?' + 'size=' + size + '&page=' + page);
    }
  }
  listFournisseur(size: number, page: number) {
    if (size == 0 && page == 0) {
      return this.http.get(this._url + '/fournisseurs');
    } else {
      return this.http.get(this._url + '/fournisseurs?' + 'size=' + size + '&page=' + page);
    }

  }

  async newlistCommandes(size: number, page: number) {
    return new Promise(async (resolve) => {
      if (size == 0 && page == 0) {
        await this.http.get(this._url + '/commandes')
          .subscribe(result => {
            if (result) {
              resolve(result)
            } else {
              resolve(null)
            }
          }, err => {
            resolve(null)
          })
      } else {
        await this.http.get(this._url + '/commandes?' + 'size=' + size + '&page=' + page)
          .subscribe(result => {
            if (result) {
              resolve(result)
            } else {
              resolve(null)
            }
          }, err => {
            resolve(null)
          })
      }

    })
  }

  async newlistClient(size: number, page: number) {
    return new Promise(async (resolve) => {
      if (size == 0 && page == 0) {
        await this.http.get(this._url + '/clients')
          .subscribe(result => {
            if (result) {
              resolve(result)
            } else {
              resolve(null)
            }
          }, err => {
            resolve(null)
          })
      } else {
        await this.http.get(this._url + '/clients?' + 'size=' + size + '&page=' + page)
          .subscribe(result => {
            if (result) {
              resolve(result)
            } else {
              resolve(null)
            }
          }, err => {
            resolve(null)
          })
      }

    })
  }

  async newlistFournisseur(size: number, page: number) {
    return new Promise(async (resolve) => {
      if (size == 0 && page == 0) {
        await this.http.get(this._url + '/fournisseurs')
          .subscribe(result => {
            if (result) {
              resolve(result)
            } else {
              resolve(null)
            }
          }, err => {
            resolve(null)
          })
      } else {
        await this.http.get(this._url + '/fournisseurs?' + 'size=' + size + '&page=' + page)
          .subscribe(result => {
            if (result) {
              resolve(result)
            } else {
              resolve(null)
            }
          }, err => {
            resolve(null)
          })
      }

    })
  }

  async newaddCommande(value: Commande) {
    return new Promise(async (resolve) => {
     await  this.http.post(this._url + '/commandes', value)
        .subscribe(result => {
          if (result) {
            resolve(result)
          } else {
            resolve(null)
          }
        }, err => {
          resolve(null)
        })
    })
  }
}