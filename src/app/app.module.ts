import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { AuthInterceptor } from './modules/AuthInterceptor';
import { ServicesProvider } from '../providers/services';
import { AuthPage } from '../pages/auth/auth';
import { LoginPage } from '../pages/login/login';
import { SecurityStorageService } from './modules/security-storage-service';
import { AddProspectPage } from '../pages/add-prospect/add-prospect';
import { ListprospectPage } from '../pages/listprospect/listprospect';
import { ComptePage } from '../pages/compte/compte';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { VilleProvider } from '../providers/ServicesVilles';
import { RegionProvider } from '../providers/ServicesRegion';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Geolocation } from '@ionic-native/geolocation';
import { ProspectProvider } from '../providers/ServicesProspect';
import { RendezvousPage } from '../pages/rendezvous/rendezvous';
import { RendezvousProvider } from '../providers/ServicesRendezvous';
import { ListRendezvousPage } from '../pages/list-rendezvous/list-rendezvous';
import { CommercialePage } from '../pages/commerciale/commerciale';
import { StockPage } from '../pages/stock/stock';
import { ProductionPage } from '../pages/production/production';
import { QrcodePage } from '../pages/qrcode/qrcode';
import { PhotoPage } from '../pages/photo/photo';
import { ProspectsPage } from '../pages/prospects/prospects';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { DetailprospectPage } from '../pages/detailprospect/detailprospect';
import { ConnectivityServiceProvider } from '../providers/connectivity-service/connectivity-service';
import { Network } from '@ionic-native/network';
import { CallNumber } from '@ionic-native/call-number';
import { DetailrendezvousPage } from '../pages/detailrendezvous/detailrendezvous';
import { LoadingControllerServicesProvider } from '../providers/loading-controller-services/loading-controller-services';
import { NotificationsProvider } from '../providers/notifications/notifications';
import { CategoriePage } from '../pages/moduleStock/categorie/categorie';
import { CommandePage } from '../pages/moduleStock/commande/commande';
import { MouvementPage } from '../pages/moduleStock/mouvement/mouvement';
import { ProduitPage } from '../pages/moduleStock/produit/produit';
import { ModulestockProvider } from '../providers/modulestock/modulestock';
import { CommandesProvider } from '../providers/commandes/commandeservices';
import { LogindesignPage } from '../pages/logindesign/logindesign';
import { OutilsmenuPage } from '../pages/outilsmenu/outilsmenu';
import { RhmenuPage } from '../pages/rhmenu/rhmenu';
import { ReglagePage } from '../pages/reglages/reglage/reglage';
import { TestPage } from '../pages/test/test';
import { EventEmitterService } from '../providers/EventEmitter';
import { HtmlDynamique } from '../providers/Servicehtmldynamique';
import { TestService } from '../providers/testservice';
import { AbsencePage } from '../pages/rh/absence/absence';
import { CongePage } from '../pages/rh/conge/conge';
import { DepartementPage } from '../pages/rh/departement/departement';
import { EmployePage } from '../pages/rh/employe/employe';
import { EvenementPage } from '../pages/rh/evenement/evenement';
import { CandidaturePage } from '../pages/rh/recrutement/candidature/candidature';
import { DocumentPage } from '../pages/rh/recrutement/document/document';
import { PostePage } from '../pages/rh/recrutement/poste/poste';
import { AvancePage } from '../pages/rh/salaire/avance/avance';
import { PaiementPage } from '../pages/rh/salaire/paiement/paiement';
import { PretPage } from '../pages/rh/salaire/pret/pret';
import { UtilsStockProvider } from '../providers/modulestock/utilsstock';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AuthPage,
    LoginPage,
    AddProspectPage,ListprospectPage,ComptePage, RendezvousPage, ListRendezvousPage,
    CommercialePage,StockPage,ProductionPage,PhotoPage,
    QrcodePage,ProspectsPage,DetailrendezvousPage,CategoriePage,ProduitPage,
    MouvementPage,ProductionPage,CommandePage,LogindesignPage,OutilsmenuPage,RhmenuPage,ReglagePage,TestPage,
    DetailprospectPage,
    AbsencePage,CongePage,DepartementPage,EmployePage,EvenementPage,CandidaturePage,DocumentPage,PostePage,AvancePage,PaiementPage,PretPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    SelectSearchableModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AuthPage,
    LoginPage,
    AddProspectPage,ListprospectPage,ComptePage, RendezvousPage, ListRendezvousPage,
    CommercialePage,StockPage,ProductionPage,PhotoPage,QrcodePage,
    MouvementPage,ProductionPage,CommandePage,
    ProspectsPage,DetailprospectPage,DetailrendezvousPage,
    CategoriePage,ProduitPage, LogindesignPage,OutilsmenuPage,RhmenuPage,ReglagePage,TestPage
    ,    AbsencePage,CongePage,DepartementPage,EmployePage,EvenementPage,CandidaturePage,DocumentPage,PostePage,AvancePage,PaiementPage,PretPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    SecurityStorageService,
    ServicesProvider,
    VilleProvider,
    RegionProvider,
    LocationAccuracy,
    AndroidPermissions,
    Geolocation,
    ProspectProvider,
    RendezvousProvider,
    QRScanner,
    ConnectivityServiceProvider,
    Network,
    CallNumber,
    LoadingControllerServicesProvider,
    NotificationsProvider,
    ModulestockProvider,
    CommandesProvider,
    EventEmitterService,
    HtmlDynamique,
    TestService,UtilsStockProvider
  ]
})
export class AppModule {}
