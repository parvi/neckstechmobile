import { Commande } from "./model.commande";
import { LineMouvement } from "./model.linemouvement";
import { EntrepriseModel } from "./model.entreprise";

export class Mouvement{
    public id?: number;
    public code?: string;
    public description?: string;
    public etat?: number;
    public total?: number=0;
    public type?: string;
    public createdDate?: any;
    public lastModifiedDate?: any;
    public commande?: Commande;
    public details: LineMouvement[]=[];
    public mouvement: Mouvement;
    public entreprise: EntrepriseModel;

}