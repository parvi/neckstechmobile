import { Categorie } from "./model.categorie";

export class Produit{
   /* public libelle : string;
    public description:string;
    public prixUnitaire: number;
    public prixVente : number;
    public quantite:number;
    public seuil: number;
    public categorie : any;*/
    public object: any;

    public id?: Number;
    public code?: string;
    public libelle?: string;
    public description?: string;
    public prixUnitaire?: number;
    public prixVente?: number;
    public quantite?: string;
    public seuil?: string;
    public image?: string;
    public categorie?: Categorie;
}