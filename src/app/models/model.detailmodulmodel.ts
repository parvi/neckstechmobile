import { ModuleModel } from "./model.modulemodel";
import { EntrepriseModel } from "./model.entreprise";


export class DetailsEntrepriseModuleModel {
  public id?: number;
  public status?: boolean;
  public archiver?: boolean;
  public entreprise: EntrepriseModel;
  public module: ModuleModel;
}