export class Prospect {
    private nom: string;
    private prenom: string;
    private adresse: string;
    private telephone: string;
    private prospect: string;
    private commentaire: string;
    private etatProspect: string;
    private latitude: string;
    private longitude: string;
    private email: string;
    private ville: string;
     
    public Prospect(){

    }

    get Nom(): string {
        return this.nom;
    }
    set Nom(value: string) {
        this.nom = value;
    }

    get Prenom(): string {
        return this.prenom;
    }
    set Prenom(value: string) {
        this.prenom = value;
    }

    get Adresse(): string {
        return this.adresse;
    }
    set Adresse(value: string) {
        this.adresse = value;
    }

    get Telephone(): string {
        return this.telephone;
    }
    set Telephone(value: string) {
        this.telephone = value;
    }

    get PProspect(): string {
        return this.prospect;
    }
    set PProspect(value: string) {
        this.prospect = value;
    }

    get Commentaire(): string {
        return this.commentaire;
    }
    set Commentaire(value: string) {
        this.commentaire = value;
    }

    get EtatProspect(): string {
        return this.etatProspect;
    }
    set EtatProspect(value: string) {
        this.etatProspect = value;
    }

    get Latitude(): string {
        return this.latitude;
    }
    set Latitude(value: string) {
        this.latitude = value;
    }

    get Longitude(): string {
        return this.longitude;
    }
    set Longitude(value: string) {
        this.longitude = value;
    }

    get Email(): string {
        return this.email;
    }
    set Email(value: string) {
        this.email = value;
    }

    get Ville(): string {
        return this.ville;
    }
    set Ville(value: string) {
        this.ville = value;
    }
}