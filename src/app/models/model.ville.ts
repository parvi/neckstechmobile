import { Region } from "./region.model";

export class Ville {
    public id?: number;
    public code?: number;
    public nom?: string;
    public region?: Region;
  }