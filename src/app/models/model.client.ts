import { EntrepriseModel } from "./model.entreprise";

export class Client{
   /* public code : string;
    public firstName:string;
    public lastName: any;
    public telephone: string;
    public entreprise:any;*/

    public id?: number;
    public code?: string;
    public firstName?: string;
    public lastName?: string;
    public nom_complet?: string;
    public telephone?: string;
    public etat?: boolean;
    public entreprise?: EntrepriseModel;
}