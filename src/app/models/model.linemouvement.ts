import { Produit } from "./model.produit";

export class LineMouvement {
    public produit: Produit;
    public quantite: number;
    public prix: number;
  }