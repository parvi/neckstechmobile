import { Produit } from "./model.produit";

export class LineCommande {
    public produit: Produit;
    public quantite: number;
    public prix: number;
  }
  