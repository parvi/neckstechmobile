export class Region{
    private _region :any;
    private _nom : string;

    get region(): any{
        return this._region;
    }
    set region(value: any){
        this._region = value;
    }
    get nom(): string{
        return this._nom;
    }
    set nom(value: string){
        this._nom = value;
    }
}