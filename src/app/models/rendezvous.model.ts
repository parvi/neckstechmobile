import { Prospect } from "./prospect.model";

export class Rendezvous {
     responsable: string;
     date: string;
     lieu: string;
     commentaires: string;
     prospect: Prospect;
     objectRendezvous:any;

    get Responsable() : string{
        return this.responsable;
    }
    set Responsable(value: string){
        this.responsable = value
    }
    get DDate() : string{
        return this.date;
    }
    set DDate(value : string){
        this.date = value;
    }
    get Lieu() : string{
        return this.lieu;
    }
    set Lieu(value: string){
        this.lieu = value;
    }
    get Commentaire() : string{
        return this.commentaires
    }
    set Commentaire(value: string){
        this.commentaires = value;
    }
    get PProspect() : Prospect{
        return this.prospect
    }
    set PProspect(value: Prospect){
        this.prospect = value;
    }
    get Objectrendezvous() : any{
        return this.objectRendezvous
    }
    set Objectrendezvous(value: any){
        this.objectRendezvous = value;
    }
}