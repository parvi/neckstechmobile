import { Pays } from "./model.pays";


export class Region {
  public id?: number;
  public nom?: string;
  public pays?: Pays;
}
