export class Ville{
    private _ville: any;
    private _nom: string;

    get ville() : any{
        return this._ville;
    }
    set ville(value: any){
        this._ville = value;
    }
    get nom() : string{
        return this._nom;
    }
    set nom(value: string){
        this._nom = value
    }
}