import { EntrepriseModel } from "./model.entreprise";

export class Categorie {
  /*  public nom : string;
    public description:string;
    public categorie: any;*/

  public id?: Number;
  public code?: string;
  public nom?: string;
  public description?: string;
  public niveau?: Number;
  public categorie?: Categorie;
  public entreprise?: EntrepriseModel;
  public createdBy?: string
  public createdDate?: string
  public lastModifiedBy?: string
  public lastModifiedDate?: string
}