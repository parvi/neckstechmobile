import { EntrepriseModel } from "./model.entreprise";
import { Client } from "./model.client";
import { Fournisseur } from "./model.fournisseur";
import { LineCommande } from "./model.linecommande";

export class Commande{
  /*  public client : any;
    public fournisseur: any;
    public quantite: number;
    public produit: any;*/

    public id?: number;
    public code?: string;
    public etat?: number;
    public total?: number;
    public createdDate?: any;
    public lastModifiedDate?: any;
    public entreprise?: EntrepriseModel;
    public client?: Client;
    public fournisseur?: Fournisseur;
    public details: LineCommande[];
    public description?: String;
    public type?: string;
}