import { DetailsEntrepriseModuleModel } from "./model.detailmodulmodel";
import { Ville } from "./ville.model";


export class EntrepriseModel {
  public id?: number;
  public libelle: string;
  public ninea: string;
  public telephone: string;
  public fax: string;
  public email: string;
  public adresse: string;
  public  slogan: string;
  public siteWeb: string;
  public photo: string;
  public chemin: string;
  public dateCreation: any;
  public ville: Ville;
  public detailEntrepriseModules: DetailsEntrepriseModuleModel[];
}