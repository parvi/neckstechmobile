import { DetailsEntrepriseModuleModel } from "./model.detailmodulmodel";

export class ModuleModel {
    public id?: number;
    public libelle: string;
    public activer: boolean;
    public archiver: boolean;
    public detailEntrepriseModules: DetailsEntrepriseModuleModel[];
  }