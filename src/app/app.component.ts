import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { Storage } from '@ionic/storage';
import { AuthPage } from '../pages/auth/auth';
import { LogindesignPage } from '../pages/logindesign/logindesign';
import { TestPage } from '../pages/test/test';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(platform: Platform, 
              statusBar: StatusBar,
             splashScreen: SplashScreen,
             private storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.storage.get("identifiants").then(utilisateur=>{
      if(utilisateur){
        this.rootPage = HomePage;
      }else{
      // this.rootPage = AuthPage;
      this.rootPage = LogindesignPage;
    //this.rootPage = TestPage;
      }
    });
  }
}

