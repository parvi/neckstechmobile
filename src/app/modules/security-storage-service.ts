import {Observable} from "rxjs";
import {StorageKeys} from "./storage-keys";
import {Injectable} from "@angular/core";
import {Storage} from "@ionic/storage";

@Injectable()
export class SecurityStorageService {

  constructor(private storage: Storage) {

  }

  getSecurityToken() {
    return this.storage.get(StorageKeys.SecurityToken)
      .then(
        data => { return data },
        error => console.error(error)
      );
  }
  getSecurityTokenAsObservable() {
    return Observable.fromPromise(this.getSecurityToken());
  }
}
