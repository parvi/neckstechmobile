import {Injectable, NgModule} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {Storage} from "@ionic/storage";

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {

  token:any;
  constructor(public _storage: Storage) {
    _storage.get('myToken').then((val) => {
      this.token = val;
     // console.log('Your age is', val);
    });
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const changedReq = req.clone({headers: req.headers.set('Authorization', 'Bearer '+this.token)});
    return next.handle(changedReq);
  }

}


/*@NgModule({
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpsRequestInterceptor, multi: true }
  ]
})
export class InterceptorModule { }
*/
