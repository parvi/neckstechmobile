import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';

import {SecurityStorageService} from "./security-storage-service";
import {AlertController} from "ionic-angular";
import { Storage } from '@ionic/storage';

import { _throw } from 'rxjs/observable/throw';
import { catchError, mergeMap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private securityStorageService: SecurityStorageService,
    private storage : Storage,
    private alertCtrl: AlertController
  ) {
  }
/*
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // This method gets a little tricky because the security token is behind a
    // promise (which we convert to an observable). So we need to concat the
    // observables.
    //  1. Get the token then use .map to return a request with the token populated in the header.
    //  2. Use .flatMap to concat the tokenObservable and next (httpHandler)
    //  3. .do will execute when the request returns

    const tokenObservable = this.securityStorageService.getSecurityTokenAsObservable().map(token => {
      return request = request.clone({
        setHeaders: {
          Accept: `application/json`,
          'Content-Type': `application/json`,
          Authorization: `Bearer ${token}`
        }
      });

    });

    return tokenObservable.flatMap((req) => {
      return next.handle(req).do((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // do stuff to the response here
      //    this.storage.set("myToken":);
        //  console.log('vous avez l acces'+event.headers.get("id_token"));
        console.log(event);
        }
      }, (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            // not authorized error .. do something
            this.storage.set('myoken',JSON.stringify(err));
            console.log("bad"+err);
          }
        }
      });
    })
  }
 */

  // Intercepts all HTTP requests!
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let promise = this.storage.get('my_token');

    return Observable.fromPromise(promise)
      .mergeMap(token => {
       // console.log(token);
        let clonedReq = this.addToken(request, token);
        return next.handle(clonedReq).pipe(
          catchError(error => {
            // Perhaps display an error for specific status codes here already?
            let msg = error.message;
            console.log(error);
            // Pass the error to the caller of the function
            return _throw(error);
          })
        );
      });
  }

  // Adds the token to your headers if it exists
  private addToken(request: HttpRequest<any>, token: any) {
    if (token) {
      let clone: HttpRequest<any>;
      clone = request.clone({
        setHeaders: {
          Accept: `application/json`,
          'Content-Type': `application/json`,
          Authorization: `Bearer ${token}`
        }
      });
      //console.log("token maj");
      //console.log(token);
      return clone;
    }

    return request;
  }
}
