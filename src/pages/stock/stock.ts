import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CategoriePage } from '../moduleStock/categorie/categorie';
import { ProduitPage } from '../moduleStock/produit/produit';
import { MouvementPage } from '../moduleStock/mouvement/mouvement';
import { CommandePage } from '../moduleStock/commande/commande';
import { HtmlDynamique } from '../../providers/Servicehtmldynamique';
import { TestPage } from '../test/test';
import { TestService } from '../../providers/testservice';
import { UtilsStockProvider } from '../../providers/modulestock/utilsstock';


@Component({
  selector: 'page-stock',
  templateUrl: 'stock.html',
})
export class StockPage {
  listmoduleStock: any = [{
    libelle: "Catégories",
    icon: "closed-captioning"
  },
  {
    libelle: "Produits",
    icon: "cube"
  },
  {
    libelle: "Mouvements",
    icon: "repeat"
  },
  {
    libelle: "Commandes",
    icon: "cart"
  }]
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public serviceHtmlDynamique: HtmlDynamique
    , public serviceTest: TestService,
    public utilityStock: UtilsStockProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StockPage');
  }
  gotTopage(libelle: string) {

    switch (libelle) {
      case "Catégories": {
        console.log("Catégories");
        //  this.navigateUrl(CategoriePage);
        this.serviceTest.listCategorie();
        this.serviceHtmlDynamique.FormByModule(libelle)
        this.navCtrl.push(TestPage);
        break;
      }
      case "Produits": {
        console.log("Produits");
        this.serviceTest.listCategorie();
        this.serviceHtmlDynamique.FormByModule(libelle)
        this.navigateUrl(TestPage);
        break;
      }
      case "Mouvements": {
        console.log("Mouvements");
        this.utilityStock.listProduit();
        this.utilityStock.listCommandes();
        this.serviceHtmlDynamique.FormByModule(libelle)
        this.navigateUrl(TestPage);
        break;
      }
      case "Commandes": {
        console.log("Commandes");
        this.utilityStock.listProduit();
        this.utilityStock.listClient();
        this.utilityStock.listFournisseur();
        this.serviceHtmlDynamique.FormByModule(libelle)
        this.navigateUrl(TestPage);
        break;
      }
      default: {
        console.log("choix invalide");
        break;
      }
    }
  }

  navigateUrl(page: any) {
    this.navCtrl.push(page);
  }

}
