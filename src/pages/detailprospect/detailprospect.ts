import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';

import { ConnectivityServiceProvider } from '../../providers/connectivity-service/connectivity-service';
import { Geolocation } from '@ionic-native/geolocation';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { CallNumber } from '@ionic-native/call-number';
import { DetailrendezvousPage } from '../detailrendezvous/detailrendezvous';
import { RendezvousProvider } from '../../providers/ServicesRendezvous';
import { Rendezvous } from '../../app/models/rendezvous.model';
import { LoadingControllerServicesProvider } from '../../providers/loading-controller-services/loading-controller-services';
import { NotificationsProvider } from '../../providers/notifications/notifications';
import { Content } from 'ionic-angular';
import * as moment from 'moment';
/**
 * Generated class for the DetailprospectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@Component({
  selector: 'page-detailprospect',
  templateUrl: 'detailprospect.html',
  queries: {
    content: new ViewChild(Content)
}
})
export class DetailprospectPage {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild(Content) content: Content;
  map: any;
  prospect: any;
  Nomprospect: string;
  mapInitialised: boolean = false;
  apiKey: any = "AIzaSyBqrBIcbvCDXbvL382KN5jCY8yjh_6zyU8";
  icon: string = "";
  marker: any;
  currentMapTrack = null;
  isTracking = false;
  trackedRoute = [];
  previousTracks = [];
  positionSubscription: Subscription;
  start = 'chicago, il';
  end = 'chicago, il';
  directionsService = null;
  directionsDisplay = null;
  rv: boolean = false;
  rendezvous: Rendezvous = new Rendezvous();
  user: string;
  conclusioncheckad: boolean;
  conclusionchecktp: boolean;
  disableButtond: boolean = true;
  iconProspet: string;
  etatRendezvous:string="";
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public connectivityService: ConnectivityServiceProvider,
    public geolocation: Geolocation,
    public storage: Storage,
    public callNumber: CallNumber,
    public platform: Platform,
    public serviceRendezvous: RendezvousProvider,
    public ServicesLoading: LoadingControllerServicesProvider,
    public ServicesNotifications: NotificationsProvider) {
    this.initData();

  }

  loadGoogleMaps() {

    this.addConnectivityListeners();

    if (typeof google == "undefined" || typeof google.maps == "undefined") {

      console.log("Google maps JavaScript needs to be loaded.");
      this.disableMap();

      if (this.connectivityService.statConnection()) {
        console.log("online, loading map");

        //Load the SDK
        window['mapInit'] = () => {
          this.initMap();
          this.enableMap();
        }

        let script = document.createElement("script");
        script.id = "googleMaps";

        if (this.apiKey) {
          script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
        } else {
          script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
        }

        document.body.appendChild(script);

      }
    }
    else {

      if (this.connectivityService.statConnection()) {
        console.log("showing map");
        this.initMap();
        this.enableMap();
      }
      else {
        console.log("disabling map");
        this.disableMap();
      }

    }

  }
  initData() {
    this.prospect = this.navParams.get("prospect") as any;
    console.log(this.prospect)
    if (this.prospect) {
      this.Nomprospect = this.prospect.prospect
      this.loadGoogleMaps();
    }

  }

  initMap() {
    this.mapInitialised = true;
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;
    this.geolocation.getCurrentPosition().then((position) => {

      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      var locations: any = []

      console.log(this.iconProspet)
      var iconPathrepar: string;
      if (this.iconProspet) {
        iconPathrepar = this.iconProspet;
      } else {
        iconPathrepar = this.prospect.iconPath.substring(7);

      }
      console.log(iconPathrepar);
      locations.push(
        { latitude: position.coords.latitude, longitude: position.coords.longitude, nature: 1, textPosition: "Ma position", icon: "./assets/imgs/marker.png" },
        { latitude: this.prospect.latitude, longitude: this.prospect.longitude, nature: 2, textPosition: "" + this.Nomprospect, icon: iconPathrepar }
      );

      if (locations) {
        for (let l of locations) {
          this.marker = new google.maps.Marker({
            position: new google.maps.LatLng(l.latitude, l.longitude),
            map: this.map,
            icon: l.icon
          });
          var infowindow = new google.maps.InfoWindow();
          var i;
          google.maps.event.addListener(this.marker, 'click', (function (marker, i) {
            return function () {
              infowindow.setContent(l.textPosition
              );
              infowindow.open(this.map, marker);
            }
          })(this.marker, i));
        }
      }


    });

  }

  disableMap() {
    console.log("disable map");
  }

  enableMap() {
    console.log("enable map");
  }

  addConnectivityListeners() {

    let onOnline = () => {

      setTimeout(() => {
        if (typeof google == "undefined" || typeof google.maps == "undefined") {

          this.loadGoogleMaps();

        } else {

          if (!this.mapInitialised) {
            this.initMap();
          }

          this.enableMap();
        }
      }, 2000);

    };

    let onOffline = () => {
      this.disableMap();
    };

    document.addEventListener('online', onOnline, false);
    document.addEventListener('offline', onOffline, false);

  }
  calculateAndDisplayRoute() {
    this.directionsService.route({
      origin: this.start,
      destination: this.end,
      travelMode: 'DRIVING'
    }, (response, status) => {
      if (status === 'OK') {
        console.log(response);
        this.directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }
  itineraire() {
    console.log("Directions request")
    this.geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.directionsDisplay.setMap(this.map);

      this.directionsService.route({
        origin: { lat: position.coords.latitude, lng: position.coords.longitude },
        //14.6929225,-17.4590014
        destination: { lat: this.prospect.latitude, lng: this.prospect.longitude },
        travelMode: 'DRIVING'
      }, (response, status) => {
        if (status === 'OK') {
          this.directionsDisplay.setDirections(response);
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });
      this.startTracking();
    })
  }
  startTracking() {
    this.isTracking = true;
    this.trackedRoute = [];

    this.positionSubscription = this.geolocation.watchPosition()
      .pipe(
        filter((p) => p.coords !== undefined) //Filter Out Errors
      )
      .subscribe(data => {
        setTimeout(() => {
          this.trackedRoute.push({ lat: data.coords.latitude, lng: data.coords.longitude });
          this.markerPosition(data.coords.latitude, data.coords.longitude)
          this.redrawPath(this.trackedRoute);
        }, 0);
      });
  }
  redrawPath(path) {
    this.icon = "./assets/imgs/marker.png";
    if (this.currentMapTrack) {
      this.currentMapTrack.setMap(null);
    }

    if (path.length > 1) {
      this.currentMapTrack = new google.maps.Polyline({
        path: path,
        geodesic: true,
        strokeColor: '#ffffff',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        icon: this.icon
      });
      this.currentMapTrack.setMap(this.map);
    }
  }
  stopTracking() {
    let newRoute = { finished: new Date().getTime(), path: this.trackedRoute };
    this.previousTracks.push(newRoute);
    this.storage.set('routes', this.previousTracks);

    this.isTracking = false;
    this.positionSubscription.unsubscribe();
    this.currentMapTrack.setMap(null);
  }
  markerPosition(lt: any, lg: any) {
    if (this.marker && this.marker.setMap) {
      this.marker.setMap(null);
    }
    let latLng = new google.maps.LatLng(lt, lg);
    this.map.setCenter(latLng);
    this.icon = "./assets/imgs/marker.png";
    var infowindow = new google.maps.InfoWindow();

    var i;
    this.marker = new google.maps.Marker({
      position: new google.maps.LatLng(lt, lg),
      map: this.map,
      icon: this.icon
    });

    google.maps.event.addListener(this.marker, 'click', (function (marker, i) {
      return function () {
        infowindow.setContent('Ma position actuelle');
        infowindow.open(this.map, marker);
      }
    })(this.marker, i));

  }
  call() {
    console.log(this.prospect.telephone)
    if (this.prospect.telephone != null && this.prospect.telephone != "") {
      if (this.platform.is("cordova")) {
        this.callNumber.callNumber(this.prospect.telephone, true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
      } else {
        console.log("cordova is missing");
      }
    }

  }
  detailsRenvousEncours() {
    this.navCtrl.push(DetailrendezvousPage, {
      prospect: this.prospect
    })
  }
  getRvenCours() {
    let load = this.ServicesLoading.showLoader();
    this.serviceRendezvous.ListRendezvousByProspectEncours(this.prospect.id)
      .subscribe(response => {
        load.dismiss();
        if (response.length !== 0) {

          this.rv = true;
          this.user = response[0].utilisateur.prenom + " " + response[0].utilisateur.prenom;
          this.rendezvous.Responsable = response[0].responsable
          this.rendezvous.DDate = response[0].date
          this.rendezvous.Lieu = response[0].lieu
          this.rendezvous.Objectrendezvous = response[0];
          this.etatRendezvous = response[0].etat;

          console.log(this.rendezvous.Objectrendezvous) ;
        } else {
          this.ServicesNotifications.presentToastMessage("Pas de rendez-vous pour ce prospect");
        }
        this.scrollToBottom();
      }, err => {
        console.log(err);
        load.dismiss();
      }
      )

    //  console.log(subs)
  }
  checkConclusion(checkname: string) {
    if (this.conclusioncheckad == this.conclusionchecktp == true) {
      if (checkname == "adherant") {
        this.conclusionchecktp = false;
        this.disableButtond = false;
        this.rendezvous.Objectrendezvous.etat = this.etatRendezvous = "adherant"
      } else {
        this.conclusioncheckad = false;
        this.disableButtond = false;
        this.rendezvous.Objectrendezvous.etat = this.etatRendezvous = "prospect"
      }
    }

  }
  validerRendezVous() {
    // console.log(this.rendezvous.Objectrendezvous)
    console.log("date prospect: " + moment(this.prospect.date).format("DD-MM-YYYY"));
    console.log("date aujourdhui: " + moment().format("DD-MM-YYYY"));
    var date1 = moment(this.prospect.date);
    var date2 = moment();
    var nbjours = date2.diff(date1, 'days');
    if (nbjours <= 7) {
      this.iconProspet = "./assets/img/map-marker-jaune.png";
    } else if (nbjours > 7 && nbjours <= 14) {
      this.iconProspet = "./assets/img/map-marker-orange.png";
    } else if (nbjours > 14) {
      this.iconProspet = "./assets/img/map-marker-rouge.png";
    }
    var loader = this.ServicesLoading.showLoader();
    if (this.rendezvous.Objectrendezvous !== null) {
      console.log(this.rendezvous)
      this.serviceRendezvous.updateRv(this.rendezvous.Objectrendezvous).
        subscribe(response => {
          loader.dismiss();
          if (response.success) {
            this.ServicesNotifications.presentToastMessage("Rendez-vous cloturé avec succès");
            this.scrollToTop();
            this.rv = false;
          }

        })
    }
  }
  scrollToTop() {
    this.content.scrollToTop();
    this.loadGoogleMaps();
  }
  scrollToBottom() {
    this.content.scrollToBottom();
    //this.content.scrollToTop();

  }
}
