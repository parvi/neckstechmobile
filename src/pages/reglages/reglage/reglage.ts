import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AuthPage } from '../../auth/auth';
import { NotificationsProvider } from '../../../providers/notifications/notifications';
import { LogindesignPage } from '../../logindesign/logindesign';


@Component({
  selector: 'page-reglage',
  templateUrl: 'reglage.html',
})
export class ReglagePage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage :Storage,
              public notifications: NotificationsProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReglagePage');
  }

  deconnexion() {
    this.storage.set("identifiants", null);
    this.storage.set("utilisateurs", null);
    this.notifications.presentToastMessage('Deconnexion');
    this.navCtrl.setRoot(LogindesignPage);

  }
}
