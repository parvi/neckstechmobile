import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AuthPage } from '../auth/auth';
import { AddProspectPage } from '../add-prospect/add-prospect';
import { ServicesProvider } from '../../providers/services';
import { Subscription } from 'rxjs';
import { ComptePage } from '../compte/compte';
import { CommercialePage } from '../commerciale/commerciale';
import { ProductionPage } from '../production/production';
import { StockPage } from '../stock/stock';
import { QrcodePage } from '../qrcode/qrcode';
import { RhmenuPage } from '../rhmenu/rhmenu';
import { OutilsmenuPage } from '../outilsmenu/outilsmenu';
import { ReglagePage } from '../reglages/reglage/reglage';
import { LogindesignPage } from '../logindesign/logindesign';
import { TestPage } from '../test/test';
import { HtmlDynamique } from '../../providers/Servicehtmldynamique';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  base64Image: any;
  _user: any = null;
  imageEntreprise: string = '';
  nomEntreprise: string = '';
  sloganEntreprise: string = '';
  constructor(public navCtrl: NavController,
    public storage: Storage,
    public toast: ToastController,
    public services: ServicesProvider,
    public serviceHtmlDynamique: HtmlDynamique) {
    this.presentToastMessage('Bienvenu(e) mr help');
    this.getUserInfos();

  }

  deconnexion() {
    this.storage.set("identifiants", null);
    this.presentToastMessage('Deconnexion');
    this.navCtrl.setRoot(LogindesignPage);

  }

  presentToastMessage(message: string) {
    const toast = this.toast.create({
      position: 'top',
      message: message,
      showCloseButton: true,
      closeButtonText: "OK",
      duration: 3000
    });
    toast.present();
  }
 /* newProspect() {
    this.navCtrl.push(AddProspectPage);
  }*/

  getUserInfos() {

    let subscribe: Subscription = this.services.getUser().subscribe(user => {
      if (user) {
        this._user = user
        if (this._user.entreprise) {
          this.imageEntreprise = this._user.entreprise.chemin;
          this.nomEntreprise = this._user.entreprise.libelle;
          this.sloganEntreprise = this._user.entreprise.slogan;
        }

      }

    }
      , error => {
        console.log(error)
      });

  }
  /* monCompte(){
     this.navCtrl.push(ComptePage);
   }
   commerciale(){
     this.navCtrl.push(CommercialePage);
   }
   production(){
     this.navCtrl.push(ProductionPage);
   }
   stock(){
     this.navCtrl.push(StockPage);
   }
   qrScaner(){
     this.navCtrl.push(QrcodePage);
   }*/
  navigationPage(page: string) {
    switch (page) {
      case 'compte':
        this.navCtrl.push(ComptePage);
        break;   
      case 'rh':
        this.navCtrl.push(RhmenuPage);
        break;
      case 'deconnexion':
        this.storage.set("identifiants", null);
        this.presentToastMessage('Deconnexion');
        this.navCtrl.setRoot(AuthPage);
        break;
      case 'stock':
        this.navCtrl.push(StockPage);
        break;
      case 'outils':
        this.navCtrl.push(OutilsmenuPage);
        break;
      case 'prospect':
        this.serviceHtmlDynamique.FormByModule(page)
        this.navCtrl.push(AddProspectPage);
        break;
      case 'reglage':
        this.navCtrl.push(ReglagePage);
        break;
        case 'test':
        this.serviceHtmlDynamique.FormByModule(page)
        this.navCtrl.push(TestPage);
        break;
    }
  }

}
