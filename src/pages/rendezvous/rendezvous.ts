import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { Rendezvous } from '../../app/models/rendezvous.model';
import { RendezvousProvider } from '../../providers/ServicesRendezvous';
import { LoadingControllerServicesProvider } from '../../providers/loading-controller-services/loading-controller-services';
import { NotificationsProvider } from '../../providers/notifications/notifications';

@Component({
  selector: 'page-rendezvous',
  templateUrl: 'rendezvous.html',
})
export class RendezvousPage {
  prospect: any;
  value : string;
  heureRendezvous:string=''
  rendezvous: Rendezvous = new Rendezvous();
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public servicesRendezvous: RendezvousProvider,
              public servicesLoad: LoadingControllerServicesProvider,
              public servicesNotification: NotificationsProvider
      ) {
       this.prospect = this.navParams.get('prospect') as any;
  }
  clearList(){

  }
  recapTime(){

  }
  valueForm(){
    /*const loading = this.loadingCtrl.create({
      content: "En cours...",
    });
    loading.present();*/
    var loader = this.servicesLoad.showLoader();
    this.rendezvous.PProspect = this.prospect;
    console.log(this.rendezvous)
    var dateRendezvous = this.rendezvous.DDate + "T" + this.heureRendezvous+"Z";
    var object = {
      responsable : this.rendezvous.Responsable,
      commentaires: this.rendezvous.Commentaire,
      prospect: this.prospect,
      date: dateRendezvous,
      lieu: this.rendezvous.Lieu
    }
    console.log(object)
    this.servicesRendezvous.ajoutRendezvous(object).
    subscribe(response=>{
      loader.dismiss();
      this.servicesNotification.presentToastMessage("Rendez-vous ajouté")
      this.init();
      console.log(response)
    },err=>{
      loader.dismiss();
      console.log(err)
      var message = 'Opération échouée';
      var title = 'Erreur'
    //  this.messageDialog(message,title)
      this.servicesNotification.presentConfirm(title, message, 'Annuler', 'OK')
    })
  }
  
 
  init(){
    this.rendezvous.DDate =null;
    this.rendezvous.Commentaire = '';
    this.rendezvous.responsable = '';
    this.rendezvous.Lieu = '';
    this.rendezvous.prospect = null;
    this.heureRendezvous = null;
  }

}
