import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EventEmitterService } from '../../providers/EventEmitter';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { TestService } from '../../providers/testservice';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { NotificationsProvider } from '../../providers/notifications/notifications';
import { UtilsStockProvider } from '../../providers/modulestock/utilsstock';
import { ModulestockProvider } from '../../providers/modulestock/modulestock';
import { Categorie } from '../../app/models/model.categorie';
import { LoadingControllerServicesProvider } from '../../providers/loading-controller-services/loading-controller-services';
import { LineMouvement } from '../../app/models/model.linemouvement';


@Component({
  selector: 'page-test',
  templateUrl: 'test.html',
})
export class TestPage {
  /* phoneNotices: Array<{ name: string, placehorlder: string, type:string }> = [
     { name: "", placehorlder: 'Nom', type: 'text'  },
     { name: "", placehorlder: 'Prenom' , type: 'text' },
     { name: "", placehorlder: 'Adresse' , type: 'text' },
     { name: "", placehorlder: 'mot de passe' , type: 'password' },
   ];*/
  item: any;
  listInput: Array<{}>;
  module: string;
  details: any;
  totaldetail: number;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public emitterevent: EventEmitterService,
    public serviceTest: TestService,
    public notificationsService: NotificationsProvider,
    public utilsStock: UtilsStockProvider,
    public servicesModuleStock: ModulestockProvider,
    public serviceLoad: LoadingControllerServicesProvider) {
  }

  ionViewDidLoad() {
    this.initForm()

  }
  initForm() {
    this.listInput = [{}];
    this.listInput = this.emitterevent.getListInput()
    var moduleField: any = this.listInput[0];
    this.module = moduleField.module;
  }
  checkbooxFonction(listfield) {
    //  console.log(listfield)
    switch (listfield[0].module) {
      case 'categorie':
        this.listInput = this.serviceTest.checkSouscategorie(this.listInput);
        break;
      case 'mouvement':
        this.listInput = this.serviceTest.checkCommande(this.listInput);
        break;
      case 'mouvement':
        this.listInput = this.serviceTest.checkCommande(this.listInput);
        break;
    }
  }
  checkbooxFonctionUniform(listfield, index, field) {
    this.listInput = this.serviceTest.checkObject(this.listInput, index, listfield[0].module, field);
  }
  portChangeFunction(event: {
    component: SelectSearchableComponent,
    value: any
  }, index: number) {
    this.checkListFunction(this.listInput, this.item, index)
  }
  checkListFunction(listfield, item, index) {
    switch (listfield[0].module) {
      case 'categorie':
        this.listInput = this.serviceTest.checkListObject(this.listInput, item, index);
        console.log(this.listInput);
        break;
      case 'produit':
        this.listInput = this.serviceTest.checkListObject(this.listInput, item, index);
        console.log(this.listInput);
        break;
      case 'mouvement':
        this.listInput = this.serviceTest.checkListObject(this.listInput, item, index);
        console.log(this.listInput);
        break;
      case 'commande':
        this.listInput = this.serviceTest.checkListObject(this.listInput, item, index);
        console.log(this.listInput);
        break;
    }
  }
  addObjetFunction(listfield) {
    let load = this.serviceLoad.showLoader();
    switch (listfield[0].module) {
      case 'categorie':
        this.addCategorie(listfield, load);
        break;
      case 'produit':
        this.addProduit(listfield, load);
        break;
      case 'mouvement':
        this.addMouvement(listfield, load);
        break;
      case 'commande':
        this.addCommande(listfield, load);
        break;
    }
  }
  addCategorie(listfield: any, load: any) {
    var control = this.utilsStock.controlFormCategorie(listfield);
    if (control == '1') {
      this.utilsStock.ajoutCategorie(listfield)
        .then(result => {
          load.dismiss()
          if (result) {
            this.listInput = this.utilsStock.initFormCategorie()
            this.notificationsService.presentConfirm('Message', 'Ajout reussi', '', 'Ok');
          } else {
            this.notificationsService.presentConfirm('Alert', control + ' Erreur de traitement', '', 'Ok');
          }
        })
      // 
    } else {
      load.dismiss()
      this.notificationsService.presentConfirm('Alert', control + ' est obligatoire', '', 'Ok');
    }
  }
  addProduit(listfield: any, load: any) {
    var control = this.utilsStock.controlFormProduit(listfield);
    if (control == '1') {
      this.utilsStock.ajoutProduit(listfield)
        .then(result => {
          load.dismiss()
          if (result) {
            this.listInput = this.utilsStock.initFormProduit()
            this.notificationsService.presentConfirm('Message', 'Ajout reussi', '', 'Ok');
          } else {
            this.notificationsService.presentConfirm('Alert', control + ' Erreur de traitement', '', 'Ok');
          }
        })

    } else {
      load.dismiss()
      this.notificationsService.presentConfirm('Alert', control + ' est obligatoire', '', 'Ok');
    }
  }
  addMouvement(listfield: any, load: any) {
    var control = this.utilsStock.controlFormMouvement(listfield);
    if (control == '1') {

      this.utilsStock.ajoutMouvement(listfield)
        .then(result => {
          load.dismiss()
          if (result) {
            this.listInput = this.utilsStock.initFormMouvement()
            this.notificationsService.presentConfirm('Message', 'Ajout reussi', '', 'Ok');
          } else {
            this.notificationsService.presentConfirm('Alert', control + ' Erreur de traitement', '', 'Ok');
          }
        })

    } else {
      load.dismiss()
      this.notificationsService.presentConfirm('Alert', control + ' est obligatoire', '', 'Ok');
    }
  }
  addCommande(listfield: any, load: any) {
    var control = this.utilsStock.controlFormCommande(listfield);
    if (control == '1') {
      load.dismiss()
      console.log(listfield)
       this.utilsStock.ajoutCommande(listfield)
         .then(result => {
           load.dismiss()
           if (result) {
             this.listInput = this.utilsStock.initFormCommande()
             this.notificationsService.presentConfirm('Message', 'Ajout reussi', '', 'Ok');
           } else {
             this.notificationsService.presentConfirm('Alert', control + ' Erreur de traitement', '', 'Ok');
           }
         })

    } else {
      load.dismiss()
      this.notificationsService.presentConfirm('Alert', control + ' est obligatoire', '', 'Ok');
    }
  }
  ajoutDetailsProduit(listfield) {
    let load = this.serviceLoad.showLoader();
    var control = this.utilsStock.controlFormDetailsProduit(listfield);
    if (control == '1') {
      load.dismiss()
      this.listInput = this.utilsStock.ajoutDetailsProduit(listfield);
      var det: any = this.listInput[7];
      var totaldetail: any = this.listInput[8];
      console.log(this.listInput)
      this.details = det.ngvar;
      this.totaldetail = parseInt(totaldetail.ngvar + "")
      console.log(this.details);

    } else {
      load.dismiss()
      this.notificationsService.presentConfirm('Alert', control + ' est obligatoire', '', 'Ok');
    }
  }

}
