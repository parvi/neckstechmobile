import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-rhmenu',
  templateUrl: 'rhmenu.html',
})
export class RhmenuPage {
  listmoduleStock:any =[{
    libelle : "Absence",
    icon: "closed-captioning"
  },
  {
    libelle : "Congés",
    icon: "happy"
  },
  {
    libelle : "Departement",
    icon: "home"
  },
  {
    libelle : "Evenements",
    icon: "calendar"
  },
  {
    libelle : "Salaires",
    icon: "logo-euro"
  },
  {
    libelle : "Recrutment",
    icon: "people"
  }]
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RhmenuPage');
  }

}
