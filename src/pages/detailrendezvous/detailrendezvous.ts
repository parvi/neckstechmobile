import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RendezvousProvider } from '../../providers/ServicesRendezvous';

/**
 * Generated class for the DetailrendezvousPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detailrendezvous',
  templateUrl: 'detailrendezvous.html',
})
export class DetailrendezvousPage {
  prospect: any;
  user:any;
  responsable:any;
  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public serviceRendezvous: RendezvousProvider) {
    this.initData();
  }

  initData(){
    this.prospect = this.navParams.get("prospect") as any;
    if(this.prospect){
      console.log("ici")
      this.getRvenCours();
    }
  }
  getRvenCours(){
    var value:any
    this.serviceRendezvous.ListRendezvousByProspectEncours(this.prospect.id)
    .subscribe(response=>{
      console.log(response);
    if(response){
      this.user =response.utilisateur.prenom +" " +response.utilisateur.prenom;
      this.responsable= response.responsable;
    }
    }, err=>{

    }
    )
 
  //  console.log(subs)
  }

}
