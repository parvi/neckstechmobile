import { Component } from '@angular/core';
import {  NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services';
import { Storage } from '@ionic/storage';
import { HomePage } from '../home/home';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  password : string ='madjindo';
  login : string='help.123@rezilux.com';
  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public alertCtrl : AlertController,
     public loadingCtrl : LoadingController,
     public services: ServicesProvider,
     public storage: Storage) {
  }
  affiche:boolean=false;
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  connect() {
    if (this.login == "" || this.password == "") {
     var message = 'Le login et le mot de passe sont obligatoires!'
     var title =  'Attention!'
        this.messageDialog(message,title)
      } else {
        const loading = this.loadingCtrl.create({
          content: "Chargement...c",
        });
        loading.present();
        var value={login:this.login,password:this.password}
        this.services.login(value)
        .subscribe(response => {
          if(response){
            console.log(response)
            loading.dismiss();
            this.storage.set('my_token', response['id_token']);
            this.storage.set('identifiants',value);
            this.navCtrl.setRoot(HomePage);
       
          }
  
          }, error1 => {
          loading.dismiss();
          console.log(error1)
          var title:string= "Attention"
          var message :string=  "Erreur de connexion"
          this.messageDialog(message,title);
          })
      }
      //this.navCtrl.setRoot(MenuPage);
  
  }

  messageDialog(message:string,title:string){
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  majAffiche(){
    if(this.affiche == false){
      this.affiche = true;
    }else {
      this.affiche = false;
    }
  }

}
