import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Produit } from '../../../app/models/model.produit';
import { ModulestockProvider } from '../../../providers/modulestock/modulestock';
import { Mouvement } from '../../../app/models/model.mouvement';
import { CommandesProvider } from '../../../providers/commandes/commandeservices';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { NotificationsProvider } from '../../../providers/notifications/notifications';
import { LoadingControllerServicesProvider } from '../../../providers/loading-controller-services/loading-controller-services';
import { LineMouvement } from '../../../app/models/model.linemouvement';

/**
 * Generated class for the MouvementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mouvement',
  templateUrl: 'mouvement.html',
})
export class MouvementPage {
  produit: Produit = new Produit();
  listProduits: Produit[] = [];
  itemProd: Produit;
  listItemsProd: Produit[] = [];
  entree: boolean;
  sortie: boolean;
  itemCom: any;
  listItemsCom: any[] = [];
  mouvement: Mouvement = new Mouvement();
  listMouvements: Mouvement[] = [];
  showSpinnerCommande: boolean = false;
  showSpinnerProduit: boolean;
  checkcommande: boolean;
  checktype:boolean;
  commandes: any=[];
  quantite:number;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public servicesModuleStock: ModulestockProvider,
    public servicesCommandes: CommandesProvider,
    public servicesLoad: LoadingControllerServicesProvider,
    public servicesNotif: NotificationsProvider) {
      this.initVar();
    this.listProduit();
    this.listCommandes();
    this.listMouvement();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MouvementPage');
  }
  listProduit() {

    this.servicesModuleStock.listProduits(0, 0).
      subscribe(response => {

        if (response) {
          console.log(response)
          if (response['content']) {
            this.listItemsProd = response['content'];
          }
        }

      }, err => {
        console.log(err);
      }, () => {

      });
  }
  listCommandes() {
    console.log(this.checkcommande);

    if (this.checkcommande) {

      this.showSpinnerCommande = true;
      this.servicesCommandes.listCommandes(0,0).
        subscribe(response => {

          if (response) {
            console.log(response)
            if(response['content']){
               for(let c of response['content']){
                 var nom :string = '';
                 if(c.client){
                  nom = c.client.nom_complet+" "+c.client.telephone;
                 }else{
                  nom = c.fournisseur.nom_complet+" "+c.fournisseur.telephone;
                 }
                var catitem = {
                  nom: nom,
                  object: c
                }
                this.commandes.push(catitem);
               }

              }
          }

        }, err => {
          console.log(err);
        }, () => {

        });
    } else {
      this.showSpinnerCommande = false;
    }

  }

  portChangeCom(event: {
    component: SelectSearchableComponent,
    value: any
  }) {
    console.log(this.itemCom);
     var select :any= this.itemCom;
     this.mouvement.commande = select.object;
  }
  portChangeProd(event: {
    component: SelectSearchableComponent,
    value: any
  }) {
    console.log(this.itemProd);
  //   var select :any= this.itemProd;
    // this.mouvement.produit = this.itemProd;
  }
  checkType(check:string) {
    console.log(check);
    if (this.entree == this.sortie == true) {
      if (check == "entree") {
        this.sortie = false;
        
      } else {
        this.entree = false;
       
      }
    }
      this.mouvement.type = check;
    

    console.log(this.mouvement)
  }
  ajoutDetails(){
  
      var details: LineMouvement = new LineMouvement();
      details.produit = this.itemProd;
      details.quantite = parseInt(this.quantite + "");
      details.prix = this.itemProd.prixVente;
      this.mouvement.details.push(details);
      this.mouvement.total += details.quantite;
      this.quantite = null;
      this.itemProd = null;
      console.log(this.mouvement);
    
  }
  addMouvement() {
  let load =  this.servicesLoad.showLoader();
  console.log(this.mouvement);
  this.servicesModuleStock.addMouvement(this.mouvement)
  .subscribe(reponse=>{
      if(reponse){
          load.dismiss();
          this.initVar();
          this.servicesNotif.presentToastMessage("Mouvement enregiste");
      }  
  },err=>{
    load.dismiss();
  },()=>{
    load.dismiss();
  })
 
  }
  initVar(){
    this.mouvement = new Mouvement();
    this.checkcommande = false;
    this.entree = false;
    this.sortie = false;
  }
  listMouvement(){
    let load =this.servicesLoad.showLoader();
    this.servicesModuleStock.listMouvement(0,0)
    .subscribe(response=>{
      if(response['content']){
        this.listMouvements = response['content'];
        load.dismiss();
      }
      console.log(response);
    },err=>{
      load.dismiss();
    })
  }
}
