import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Produit } from '../../../app/models/model.produit';
import { ModulestockProvider } from '../../../providers/modulestock/modulestock';
import { LoadingControllerServicesProvider } from '../../../providers/loading-controller-services/loading-controller-services';
import { NotificationsProvider } from '../../../providers/notifications/notifications';
import { Categorie } from '../../../app/models/model.categorie';
import { SelectSearchableComponent } from 'ionic-select-searchable';

/**
 * Generated class for the ProduitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-produit',
  templateUrl: 'produit.html',
})
export class ProduitPage {
  produit: Produit = new Produit();
  listProduits: Produit[]= [];
  itemProd:Produit;
  listItemsProd:any[] = [];
  spinner:boolean = true;
  showSpinnerCat:boolean;
  
  categorie: Categorie = new Categorie();
  listCategories: Categorie[]= [];
  itemCat:Categorie;
  listItemsCat:any[] = [];
  
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public servicesModuleStock: ModulestockProvider,
    public servicesLoad: LoadingControllerServicesProvider,
    public servicesNotifs: NotificationsProvider) {
      this.listCategorie();
      this.listProduit();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProduitPage');
  }
  listCategorie(){
    console.log("et la list s affiche")
 
   this.servicesModuleStock.listCategories(10,0).
    subscribe(response=>{
      this.spinner = false;
      if(response){
        if(response['content']){
         for(let c of response['content']){
       //   this.listCategories = response['content'];
          var cat : Categorie = new Categorie();
          cat.nom = c.nom;
          cat.description = c.description;
          this.listCategories.push(c);
          var catitem = {
            nom: c.nom,
            description: c.description,
            object: c
          }
          this.listItemsCat.push(catitem);
         }
         
          console.log( this.listCategories);
        }
      }
 
    },err=>{
      console.log(err);
    },()=>{
 
    });
  }
  addProduit(){
    console.log(this.produit);
   let load= this.servicesLoad.showLoader();
   if(this.produit){
    this.servicesModuleStock.addProduit(this.produit).
    subscribe(response=>{
      if(response){
        console.log(response)
           this.servicesNotifs.presentToastMessage("produit ajouté");
           this.listProduits.push(this.produit)
           this.initVar();
      }
    },err=>{
     load.dismiss();
      console.log(err)
    },()=>{
     load.dismiss();
    });
   }else{
     load.dismiss();
   }
    
  }
  listProduit(){
     
   this.servicesModuleStock.listProduits(10,0).
   subscribe(response=>{
     this.spinner = false;
     if(response){
       console.log(response)
       if(response['content']){
        for(let c of response['content']){
          
        var prod : Produit = new Produit();
         prod.libelle = c.libelle;
         prod.description = c.description;
         prod.categorie = c.categorie;
         prod.prixUnitaire = c.prixUnitaire;
         prod.prixVente = c.prixVente;
         prod.seuil = c.seuil;
         prod.quantite = c.quantite;
         this.listProduits.push(prod);
        
        }
     
       }
     }

   },err=>{
     console.log(err);
   },()=>{

   });
  }
  portChangeCat(event: {
    component: SelectSearchableComponent,
    value: any
  }) {
   console.log(this.itemCat);
   var select :any= this.itemCat;
   this.categorie.categorie = select.object;
   this.produit.categorie = select.object;
   console.log(this.categorie);
  }
  initVar(){
    this.produit.categorie = null;
    this.produit.description=null;
    this.produit.libelle=null;
    this.produit.prixUnitaire = null;
    this.produit.prixVente = null;
    this.produit.seuil = null;
    this.produit.quantite=null;
    this.itemCat = null;
  }
}
