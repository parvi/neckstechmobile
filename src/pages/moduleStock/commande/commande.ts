import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Commande } from '../../../app/models/model.commande';
import { CommandesProvider } from '../../../providers/commandes/commandeservices';
import { Produit } from '../../../app/models/model.produit';
import { ModulestockProvider } from '../../../providers/modulestock/modulestock';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { Client } from '../../../app/models/model.client';
import { Fournisseur } from '../../../app/models/model.fournisseur';
import { NotificationsProvider } from '../../../providers/notifications/notifications';
import { LoadingControllerServicesProvider } from '../../../providers/loading-controller-services/loading-controller-services';
import { ThrowStmt } from '@angular/compiler';
import { LineCommande } from '../../../app/models/model.linecommande';

/**
 * Generated class for the CommandePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-commande',
  templateUrl: 'commande.html',
})
export class CommandePage {
  clientcheck: boolean;
  fournisseurcheck: boolean;
  commande: Commande = new Commande();
  itemProd: Produit;
  listItemsProd: Produit[] = [];
  itemClient: Client = null;
  listItemsClient: Client[] = [];
  itemFournisseur: Fournisseur = null;
  listItemsFournisseur: Fournisseur[] = [];
  client: Client = new Client();
  fournisseur: Fournisseur = new Fournisseur();
  clientSelected: any;
  fournisseurSelected: any;
  quantite: number;
  commandes: Commande[] = [];
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public servicesCommande: CommandesProvider,
    public servicesModuleStock: ModulestockProvider,
    public notifsServices: NotificationsProvider,
    public load: LoadingControllerServicesProvider) {
      this.listCommande();
    this.initVar();
    this.listProduit();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommandePage');
  }
  ajouterDetails() {
    console.log(this.itemClient)
    console.log(this.itemFournisseur)
    if (this.itemClient == this.itemFournisseur == null) {
      this.notifsServices.presentToastMessage("Choisir d'abord un client ou un fournisseur");
    } else {

      var details: LineCommande = new LineCommande();
      details.produit = this.itemProd;
      details.quantite = parseInt(this.quantite + "");
      if (this.itemClient) {
        details.prix = this.itemProd.prixVente;
      } else {
        details.prix = this.itemProd.prixUnitaire;
      }

      this.commande.details.push(details);
      this.commande.total += details.quantite;
      this.quantite = null;
      this.itemProd = null;
      console.log(this.commande);
    }

  }
  addCommande() {
    let load = this.load.showLoader();
    this.servicesCommande.addCommande(this.commande)
      .subscribe(reponse => {
        if (reponse) {
          this.initVar();
          this.notifsServices.presentToastMessage("Commande ajoutée");
          load.dismiss();
        }
      },
        err => {
          load.dismiss();
        })
    console.log(this.commande);
  }
  listClient() {
    if (this.clientcheck) {
      this.fournisseurcheck = false;
      this.listItemsClient = [];
      this.servicesCommande.listClient(0, 0)
        .subscribe(response => {
          if (response) {
            console.log(response)
            if (response['content']) {
              for (let c of response['content']) {

                var catitem = {
                  nom: c.nom_complet,
                  object: c
                }
                this.listItemsClient.push(c);
              }

            }
          }
        })
    }
  }
  listFournisseur() {
    if (this.fournisseurcheck) {
      this.clientcheck = false;
      this.listItemsFournisseur = [];
      this.servicesCommande.listFournisseur(0, 0)
        .subscribe(response => {

          if (response) {
            console.log(response)
            if (response['content']) {
              for (let c of response['content']) {
                var catitem = {
                  nom: c.nom_complet,
                  object: c
                }
                this.listItemsFournisseur.push(c);
              }

            }
          }
        })
    }
  }
  listProduit() {

    this.servicesModuleStock.listProduits(0, 0).
      subscribe(response => {
        if (response) {
          console.log(response)
          if (response['content']) {
            for (let c of response['content']) {
              var catitem = {
                nom: c.libelle,
                description: c.description,
                object: c
              }
              this.listItemsProd.push(c);
            }
          }
        }
      }, err => {
        console.log(err);
      }, () => {

      });
  }
  portChangeProd(event: {
    component: SelectSearchableComponent,
    value: any
  }) {
    console.log(this.itemProd);
    var select: any = this.itemProd;
  }
  portChangeCl(event: {
    component: SelectSearchableComponent,
    value: any
  }) {
    console.log(this.itemClient);
    this.commande.client = this.itemClient;
    this.itemFournisseur = null;
    /*  var select :any= this.itemClient;
      this.commande.fournisseur = null;
      this.commande.client = select;*/
  }

  portChangeFr(event: {
    component: SelectSearchableComponent,
    value: any
  }) {
    console.log(this.itemFournisseur);
    this.commande.fournisseur = this.itemFournisseur;
    this.itemClient = null;
    /* var select :any= this.itemFournisseur;
     this.commande.client = null;
     this.commande.fournisseur = select;*/
  }
  initVar() {
    this.commande.details = [];
    this.commande.client = null;
    this.commande.total = null;
    this.clientcheck = false;
    this.fournisseurcheck = false;
    
  }
  listCommande(){
    this.servicesCommande.listCommandes(0,0)
    .subscribe(reponse=>{ 
     // console.log(reponse);
      if (reponse) {
        console.log(reponse)
        if (reponse['content']) {
          for (let c of reponse['content']) {
            var catitem = {
              nom: c.nom_complet,
              object: c
            }
            this.commandes.push(c);
          }
        }
        console.log(this.commandes);
      }
    });
  }

}
