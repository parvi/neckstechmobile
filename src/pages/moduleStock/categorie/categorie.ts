import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModulestockProvider } from '../../../providers/modulestock/modulestock';
import { Categorie } from '../../../app/models/model.categorie';
import { ServicesProvider } from '../../../providers/services';
import { LoadingControllerServicesProvider } from '../../../providers/loading-controller-services/loading-controller-services';
import { NotificationsProvider } from '../../../providers/notifications/notifications';
import { SelectSearchableComponent } from 'ionic-select-searchable';

/**
 * Generated class for the CategoriePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-categorie',
  templateUrl: 'categorie.html',
})
export class CategoriePage {
 
  categorie: Categorie = new Categorie();
  listCategories: Categorie[]= [];
  itemCat:Categorie;
  listItemsCat:any[] = [];
  spinner:boolean = true;
  showSpinnerCat:boolean;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public servicesModuleStock: ModulestockProvider,
              public servicesLoad: LoadingControllerServicesProvider,
              public servicesNotifs: NotificationsProvider) {
                this.listCategorie();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriePage');
  }
  ajoutCategorie(){
   let load =  this.servicesLoad.showLoader();
    if(this.categorie){
      this.servicesModuleStock.addCategorie(this.categorie).
      subscribe(response=>{
        
        if(response){
          this.servicesNotifs.presentToastMessage("catégorie ajoutée");
          this.listCategories.push(this.categorie);
          this.categorie = new Categorie();
        }
        
      },err=>{
        load.dismiss();
        console.log(err);
      },()=>{
        load.dismiss();
      });
    }
  }
  listCategorie(){
    console.log("et la list s affiche")
 
   this.servicesModuleStock.listCategories(10,0).
    subscribe(response=>{
      this.spinner = false;
      if(response){
        if(response['content']){
         for(let c of response['content']){
       //   this.listCategories = response['content'];
          var cat : Categorie = new Categorie();
          cat.nom = c.nom;
          cat.description = c.description;
          this.listCategories.push(c);
          var catitem = {
            nom: c.nom,
            description: c.description,
            object: c
          }
          this.listItemsCat.push(catitem);
         }
         
          console.log( this.listCategories);
        }
      }
 
    },err=>{
      console.log(err);
    },()=>{
 
    });
  }
  checkSouscategorie(){
    if(!this.showSpinnerCat){
      this.showSpinnerCat  = true;
    }else{
      this.showSpinnerCat  = false;
    }
    
  }
  
  portChangeCat(event: {
    component: SelectSearchableComponent,
    value: any
  }) {
   console.log(this.itemCat);
   var select :any= this.itemCat;
   this.categorie.categorie = select.object;
   console.log(this.categorie);
  }

  
}
