import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ModalOptions } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the AuthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
})
export class AuthPage {

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public modalCtrl: ModalController,
     public storage : Storage) {
  }

  ionViewDidLoad() {
   
  }
  connection(){
    this.storage.get("identifiants").
    then(identifants=>{
        if(identifants){
          this.navCtrl.setRoot(HomePage);
        }else{
          this.showLoginModal();
        }
    })
    
  }
  showLoginModal() {
    const modalOptions: ModalOptions = {
      cssClass: "signInModal"
    };
    const modal = this.modalCtrl.create(LoginPage, {}, modalOptions);
    modal.present();
  }

}
