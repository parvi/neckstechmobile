import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProspectProvider } from '../../providers/ServicesProspect';
import { Prospect } from '../../app/models/prospect.model';
import { RendezvousPage } from '../rendezvous/rendezvous';
import { DetailprospectPage } from '../detailprospect/detailprospect';

@Component({
  selector: 'page-list-rendezvous',
  templateUrl: 'list-rendezvous.html',
})
export class ListRendezvousPage {
   spinnerList: boolean = true;
   prospects: any[];
   prospectsTempon: any[];
   searchTerm: string = "";
  constructor(public navCtrl: NavController,
             public navParams: NavParams,
             public serviceProspect: ProspectProvider) {
               this.listProspect();
  }
  ionViewWillEnter(){
    this.listProspect();
  }
  listProspect(){
   this.serviceProspect.ListProspect().
   subscribe(response=>{
     console.log(response)
     this.spinnerList = false;
     if(response){
      this.prospects = [];
      this.prospectsTempon  = this.prospects = response;
       
   /*    for(let r of response){
        var p : Prospect=new Prospect();
        p.PProspect =r.prospect
        p.Nom = r.nom
        p.Prenom = r.prenom;
        p.Telephone = r.telephone;
        p.Adresse = r.adresse;
        p.Email = r.email;
        p.Commentaire = r.commentaire;
        p.EtatProspect = r.etatProspect;
        p.Latitude = r.latitude;
        p.Longitude = r.longitude;
        p.Ville = r.ville.ville;
        this.prospects.push(p);
      }*/
     }
     console.log(response);
   })
  }

  creerRendezvous(item:any){
    this.navCtrl.push(RendezvousPage,{
      prospect : item
    })
  }
  recherchePrsospect(){
      var tempon = this.filtreItem(this.searchTerm);
      if(tempon){
        console.log(tempon)
          this.prospects = tempon
      }
  }
  filtreItem(searchTerm){
    this.prospects = this.prospectsTempon
    return this.prospects.filter((item)=>{
      return item.prospect.toLowerCase().includes(searchTerm.toLowerCase());
    })
  }


  detailpage(item:any){
      this.navCtrl.push(DetailprospectPage,{
        prospect: item
      });
    }
}
