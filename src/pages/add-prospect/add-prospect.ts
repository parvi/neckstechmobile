import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Prospect } from '../../app/models/prospect.model';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { Region } from '../../app/models/region.model';
import { Ville } from '../../app/models/ville.model';
import { VilleProvider } from '../../providers/ServicesVilles';
import { RegionProvider } from '../../providers/ServicesRegion';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Geolocation } from '@ionic-native/geolocation';
import { ProspectProvider } from '../../providers/ServicesProspect';
import { RendezvousPage } from '../rendezvous/rendezvous';
import { ListRendezvousPage } from '../list-rendezvous/list-rendezvous';
import { NotificationsProvider } from '../../providers/notifications/notifications';
import { LoadingControllerServicesProvider } from '../../providers/loading-controller-services/loading-controller-services';

@Component({
  selector: 'page-add-prospect',
  templateUrl: 'add-prospect.html',
})
export class AddProspectPage {
  prospect: Prospect = new Prospect();
  regions: Region[] = [];
  villes: Ville[] = [];
  vil: Ville;
  reg: Region;
  villeChoisi: any = null;
  region: any = null;
  showSpinnerVille: boolean = true;
  showSpinnerRegion: boolean = true;
  locationCoords: any;
  value: string = "Test ici";
  prospectapi: any = null;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public servicesVille: VilleProvider,
    public servicesRegion: RegionProvider,
    private androidPermissions: AndroidPermissions,
    private locationAccuracy: LocationAccuracy,
    private geolocation: Geolocation,
    private servicesProspect: ProspectProvider,
    private servicesLoad: LoadingControllerServicesProvider,
    public servicesNotification: NotificationsProvider) {
    this.initCoordonnees();
    this.checkGPSPermission();
    this.listRegion();
  }
  initCoordonnees() {
    this.locationCoords = {
      latitude: "",
      longitude: "",
      accuracy: "",
      timestamp: ""
    }
  }
  portChangeVille(event: {
    component: SelectSearchableComponent,
    value: any
  }) {
    this.prospect.Ville = "" + this.vil.ville.id;
  }
  portChangeRegion(event: {
    component: SelectSearchableComponent,
    value: any
  }) {
    // this.nameRegion = this.reg.name;
    this.region = this.reg.region;
    this.villes = [];
    this.vil = null;
    this.listVilleByRegionID();
  }
  listVilleByRegionID() {
    this.servicesVille.listVilleByRegionID(this.region.id).
      subscribe(response => {
        if (response) {
          this.showSpinnerVille = false
          console.log(response);
          for (let v of response) {
            var object: Ville = new Ville;
            object.nom = v.nom;
            object.ville = v
            this.villes.push(object);
          }
        } else {

          this.showSpinnerVille = false
        }

      },
        error1 => {

          this.showSpinnerVille = false
          console.log(error1);
        })
  }
  listRegion() {
    this.servicesRegion.listRegion().subscribe(response => {
      //this.storage.get("regionsPull").then((regions)=>{
      if (response) {
        console.log(response)
        this.showSpinnerRegion = false
        this.regions = [];
        for (let r of response) {
          var object: Region = new Region();
          object.nom = r.nom;
          object.region = r;
          this.regions.push(object);
        }
      } else {
        //loading.dismiss()
        this.showSpinnerRegion = false
      }
    }, error => {
      // loading.dismiss()
      this.showSpinnerRegion = false
      console.log(error)
    })
  }
  activeSubmit() {

  }
  valueForm() {
    /*const loading = this.loadingCtrl.create({
      content: "En cours...",
    });
    loading.present();*/
    var loader = this.servicesLoad.showLoader();
    this.prospect.EtatProspect = 'prospect';
    if (this.locationCoords.latitude !== '' || this.locationCoords.longitude !== '') {
      this.prospect.Latitude = this.locationCoords.latitude;
      this.prospect.Longitude = this.locationCoords.longitude;

      this.servicesProspect.ajoutProspect(this.prospect).
        subscribe(response => {
          loader.dismiss(this.prospect);
          if (response) {
            this.prospectapi = response;
            this.initVar();
            var title: string = 'Message';
            var message: string = 'Prospect ajouté. Voulez-vous ajouter un rendez-vous?';
            this.servicesNotification.presentConfirm(title, message, 'Non', 'OUI').then(res => {
              if (res === 'ok') {
                var pp: Prospect = this.prospect
                this.navCtrl.push(RendezvousPage, {
                  prospect: this.prospectapi
                })
              } else {

              }
            });

          }

        }, err => {
          loader.dismiss();
          var message = 'Opération echouée';
          var title = 'Erreur';
          this.servicesNotification.presentConfirm(title, message, 'Annuler', 'OK')
          console.log(err);
        })
    } else {
      loader.dismiss();
      var message = 'Veuillez recuperer la position de l\'entreprise';
      var title = 'Message';
      this.servicesNotification.presentConfirm(title, message, 'Annuler', 'OK')
     
    }

  }
  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              //Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }
  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        this.getLocationCoordinates()
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }
  // Methos to get device accurate coordinates using device GPS
  getLocationCoordinates() {
    this.initCoordonnees();
    this.geolocation.getCurrentPosition().then((resp) => {
      this.locationCoords.latitude = "" + resp.coords.latitude;
      this.locationCoords.longitude = "" + resp.coords.longitude;
      this.locationCoords.accuracy = "" + resp.coords.accuracy;
      this.locationCoords.timestamp = "" + resp.timestamp;
      this.servicesNotification.presentToastMessage("Latitude: " + this.locationCoords.latitude + "Longitude: " + this.locationCoords.longitude);
    }).catch((error) => {
      alert('Error getting location' + error);
    });
  }
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {

          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else {

          //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        alert(err);
      }
    );
  }




  listProspect() {
    this.navCtrl.push(ListRendezvousPage);
  }
  initVar() {
    this.prospect.Adresse = '';
    this.prospect.Commentaire = '';
    this.prospect.Email = '';
    this.prospect.Latitude = '';
    this.prospect.Longitude = '';
    this.prospect.Nom = '';
    this.prospect.Prenom = '';
    this.prospect.PProspect = '';
    this.prospect.Telephone = '';
    this.prospect.EtatProspect = '';
    this.reg = null;
    this.vil = null;
  }
 

}
